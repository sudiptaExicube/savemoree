// App used client icon
const Icons={
  applogo:require('../assets/icons/app_logo.png'),
  facebook_icon:require('../assets/icons/facebook_icon.png'),
  google_icon:require('../assets/icons/google_icon.png'),
  camera_icon:require('../assets/icons/camera_icon.png'),
  pickup_icon:require('../assets/icons/pickup_icon.png'),
  drop_icon:require('../assets/icons/drop_icon.png'),
  marker_icon : require('../assets/icons/marker_icon.png'),
  pickup_marker_icon : require('../assets/icons/pickup_marker.png'),
  drop_marker_icon : require('../assets/icons/drop_marker.png'),
}

// App used client images
const Images={
    loginbackground:require('../assets/images/login_background.png'),
    dashed_border : require('../assets/images/dashed_border.png'),
    profile_image:require('../assets/images/profile_image.png'),  

    zomato_food_icon:require('../assets/icons/zomato_icon.png'),
    swiggy_food_icon:require('../assets/icons/swiggy_icon.png'),
    uber_food_icon:require('../assets/icons/uber_eats_icon.png'),
    ola_ride_icon:require('../assets/icons/ola_icon.png'),
    uber_ride_icon:require('../assets/icons/uber_icon.png'),
    indriver_ride_icon:require('../assets/icons/indriver.png'),
    merucab_icon:require('../assets/icons/merucab_icon.png'),
    magicpic_icon:require('../assets/icons/magicpic_icon.png'),
    eatsure_icon:require('../assets/icons/eatsure_icon.png'),
    // Medicine
    apollo_icon:require('../assets/icons/apollo.png'),
    pharmeasy_icon:require('../assets/icons/pharmeasy.png'),
    netmed_icon:require('../assets/icons/netmed.png'),
    truemeds_icon:require('../assets/icons/truemeds.png'),
    onemg_icon:require('../assets/icons/onemg.png'),

}

export {Icons,Images}