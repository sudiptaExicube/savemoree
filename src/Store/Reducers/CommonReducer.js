import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  isDarkMode: false,
  userData:{},
  userToken:"",
  baseUrl: 'https://ourclientproject.co.in/savemore/api/',
  mapApiKey: 'AIzaSyBOqfqXfRYmyF1Hrvqyqc08nzR67m_j8Aw',
  tempEmailorMobile:"",
  // baseUrl:'https://rootapi.electropayroll.in/api/PayrollApi/',

  //Navigation state
  isStackHeaderVisible: false,
};

export const commonReducer = createSlice({
  name: 'common',
  initialState,
  reducers: {
    setIsDarkMode: (state, action) => {
      state.isDarkMode = action.payload;
    },

    saveData: (state, action) => {
      console.log("csdkbcsdkbcdsbck")
      // state.userData = action.payload;
    },

    setTempEmail: (state, action) => {
      state.tempEmailorMobile = action.payload;
    },

    setUserData: (state, action) => {
      console.log("action data : ", action.payload)
      state.userData = action.payload.data;
      state.userToken = action.payload.token;
    },

    clearUserData: (state) => {
      state.userData = {}
      state.userToken = ""
    },

    localstorage_TokenAdd: (state, action) => {
      state.userToken = action.payload;
    },
    localstorage_UserdetailsAdd: (state, action) => {
      state.userData = action.payload;
    },
    

    //Navigation State
    setIsStackHeaderVisible: (state, action) => {
      state.isStackHeaderVisible = action.payload;
    },




    
  },
});

// Action creators are generated for each case reducer function
export const {
  setIsDarkMode,
  saveData,
  setTempEmail,
  setUserData,
  clearUserData,
  localstorage_TokenAdd,
  localstorage_UserdetailsAdd,
  

  //Navigation State
  setIsStackHeaderVisible
} = commonReducer.actions;

export default commonReducer.reducer;
