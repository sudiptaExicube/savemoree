import {createSlice} from '@reduxjs/toolkit';

const initialState = {

  pickupAddress:'',
  dropAddress:'',
  red_pickupLoc:{
    latitude:'',
    longitude:''
  },
  red_dropLoc:{
    latitude:'',
    longitude:''
  }

};

export const locationReducer = createSlice({
  name: 'common',
  initialState,
  reducers: {
    reducer_setPickup: (state, action) => {
      console.log(" reducer_setPickup action data : ", action,action.payload.lat)
      state.red_pickupLoc.latitude = action.payload.lat
      state.red_pickupLoc.longitude = action.payload.lng
    },
    reducer_setPickupAddress: (state, action) => {
      console.log("action pickup data : ", action)
      state.pickupAddress = action.payload
    },

    reducer_setDrop: (state, action) => {
      state.red_dropLoc.latitude = action.payload.lat
      state.red_dropLoc.longitude = action.payload.lng
    },
    reducer_setDropAddress: (state, action) => {
      state.dropAddress = action.payload
    },

  },
});

// Action creators are generated for each case reducer function
export const {
  reducer_setPickup,
  reducer_setDrop,

  reducer_setPickupAddress,
  reducer_setDropAddress

  

} = locationReducer.actions;

export default locationReducer.reducer;
