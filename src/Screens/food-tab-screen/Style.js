//import liraries
import React, {Component} from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import { FontFamily, FontSize } from '../../Constants/Fonts';
import { useTheme } from '../../Constants/Theme/Theme';


// create a component
const Styles = () => {
  const {colorTheme} = useTheme();
  return StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      // backgroundColor: colorTheme.backGroundColor,
    },
    backgroundImageView:{width: Dimensions.get('window').width, height: Dimensions.get('window').height,resizeMode:'contain'},

    mainContainer:{
      height: Dimensions.get('window').height - 60,
      backgroundColor: '#ececec'
    },
    sectionOne:{ flex: 1 },
    sectionTwo:{
      height: 80, backgroundColor: '#fff', flexDirection: 'column',
      justifyContent: 'center',
      paddingLeft: 10, paddingRight: 10,
    },
    secTwoMain:{ width: '100%', flexDirection: 'row', justifyContent: 'center' },
    customImage:{ height: '100%', width: '100%', resizeMode: 'cover', borderRadius: 10 }


  });
};
export default Styles;