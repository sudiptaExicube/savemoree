//import liraries
import React, { createRef, useEffect, useState } from 'react';
import { View, Text, Image, Dimensions } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Styles from './Style';
// import { useTheme } from '../../../Constants/Theme/Theme';
import { useTheme } from '../../Constants/Theme/Theme';
import { ScreenLayout } from '../../Components';
import { Icons, Images } from '../../Constants/ImageIconContant';
import { WebView } from 'react-native-webview';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';

const FoodTabScreen = props => {
	// const { colorTheme } = useTheme();
	const styles = Styles();
	const dispatch = useDispatch();
	const [webviewType, setWebviewType] = React.useState('magicpic');
	const zomatoLink = "https://www.zomato.com/";
	const swiggyLink = "https://www.swiggy.com/";
	const ubereatsLink = "https://www.ubereats.com/";
	const magicpicLink = "https://magicpin.in/"
	const eatsureLink = "https://www.eatsure.com/";






	return (
		<ScreenLayout
			isHeaderShown={false}
			isShownHeaderLogo={true}
			headerTitle="hello"
			hamburgmenuVisable={true}
			showBackbutton={false}
			headerbackClick={() => { alert("back button clicked") }}
		>
			<View style={styles.mainContainer}>
				<View style={styles.sectionOne}>
					{webviewType == 'zomato' ?
						<View style={{ flex: 1 }}>
							<WebView
								source={{ uri: zomatoLink }}
								startInLoadingState={true}
								javaScriptEnabled={true}
								domStorageEnabled={true}
							/>
							<View style={{ width: '100%', position: 'absolute', top: 0, height: '100%', opacity: 0.7, backgroundColor: '#fff' }}></View>
						</View>
						:
						// webviewType == 'ubereats'?
						// 	<WebView 
						// 		source={{ uri: ubereatsLink }} 
						// 		startInLoadingState={true} 
						// 		javaScriptEnabled = {true}
						//     domStorageEnabled = {true}
						// 	/>
						// :
						webviewType == 'magicpic' ?
							<WebView
								source={{ uri: magicpicLink }}
								startInLoadingState={true}
								javaScriptEnabled={true}
								domStorageEnabled={true}
							/>
							:
							webviewType == 'eatsure' ?
								<WebView
									source={{ uri: eatsureLink }}
									startInLoadingState={true}
									javaScriptEnabled={true}
									domStorageEnabled={true}
								/>
								:

								webviewType == 'swiggy' ?
									<WebView
										source={{ uri: swiggyLink }}
										startInLoadingState={true}
										javaScriptEnabled={true}
										domStorageEnabled={true}
									/>
									:
									<></>
					}


				</View>
				<View
					style={styles.sectionTwo}>
					{/* <ScrollView horizontal={true} style={{width:'100%',alignSelf:'center'}}> */}
					<View style={styles.secTwoMain}>
						<TouchableOpacity
							onPress={() => { setWebviewType('magicpic') }}
							style={{
								height: 55, width: 55, borderRadius: 10,

								borderWidth: webviewType == 'magicpic' ? 1 : 0,
								borderColor: '#1d2d53',
								padding: webviewType == 'magicpic' ? 10 : 0
							}}>
							<Image source={Images.magicpic_icon} style={styles.customImage} />

						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => { setWebviewType('eatsure') }}
							style={{
								height: 55, width: 55, borderRadius: 10, marginLeft: 15,

								borderWidth: webviewType == 'eatsure' ? 1 : 0,
								borderColor: '#1d2d53',
								padding: webviewType == 'eatsure' ? 10 : 0
							}}>
							<Image source={Images.eatsure_icon} style={styles.customImage} />

						</TouchableOpacity>

						<TouchableOpacity
							onPress={() => { setWebviewType('swiggy') }}
							style={{
								// height:55,width:55,borderRadius:10,
								height: 55, width: 55, borderRadius: 10, marginLeft: 15, marginRight: 15,

								borderWidth: webviewType == 'swiggy' ? 1 : 0,
								borderColor: '#1d2d53',
								padding: webviewType == 'swiggy' ? 10 : 0
							}}>
							<Image source={Images.swiggy_food_icon} style={styles.customImage} />
						</TouchableOpacity>

						<TouchableOpacity
							onPress={() => { setWebviewType('zomato') }}
							style={{
								height: 55, width: 55, borderRadius: 10,

								borderWidth: webviewType == 'zomato' ? 1 : 0,
								borderColor: '#1d2d53',
								padding: webviewType == 'zomato' ? 10 : 0
							}}>
							<Image source={Images.zomato_food_icon} style={styles.customImage} />


						</TouchableOpacity>


					</View>
					{/* </ScrollView> */}
				</View>

			</View>

		</ScreenLayout>
	);
};

//make this component available to the app
export default FoodTabScreen;
