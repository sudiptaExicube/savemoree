//import liraries
import React, {Component} from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import { FontFamily, FontSize } from '../../Constants/Fonts';
import { useTheme } from '../../Constants/Theme/Theme';


// create a component
const Styles = () => {
  const {colorTheme} = useTheme();
  return StyleSheet.create({
    mainContainer:{
      height: Dimensions.get('window').height - 60,
      backgroundColor: '#ececec'
    },
    segmentOne:{
      flex: 1
    },
    segmentTwo:{
      height: 80, backgroundColor: '#fff', flexDirection: 'column',
      justifyContent: 'center',
      paddingLeft: 10, paddingRight: 10
    },
    segTwoMain:{ width: '100%', flexDirection: 'row', justifyContent: 'center' },





  });
};
export default Styles;