//import liraries
import React, { createRef, useEffect, useState } from 'react';
import { View, Text, Image, Dimensions } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Styles from './Style';
// import { useTheme } from '../../../Constants/Theme/Theme';
import { useTheme } from '../../Constants/Theme/Theme';
import { ScreenLayout } from '../../Components';
import { Icons, Images } from '../../Constants/ImageIconContant';
import { WebView } from 'react-native-webview';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';

const MedTabScreen = props => {
	// const { colorTheme } = useTheme();
	const styles = Styles();
	const dispatch = useDispatch();
	const [webviewType, setWebviewType] = React.useState('apollo');
	const ubereatsLink = "https://www.ubereats.com/";
	const magicpicLink = "https://magicpin.in/"
	const eatsureLink = "https://www.eatsure.com/";
	const swiggyLink = "https://www.swiggy.com/";
	const zomatoLink = "https://www.zomato.com/";

	const apolloLink = "https://www.apollopharmacy.in/";
	const pharmeasyLink = "https://pharmeasy.in/";
	const netmedsLink = "https://m.netmeds.com/healthstore?source_attribution=ADW-CPC-Search-NMS-Brand-NC&utm_source=ADW-CPC-Search-NMS-Brand-NC&utm_medium=CPC&utm_campaign=ADW-CPC-Search-NMS-Brand-NC&gclid=CjwKCAiAvK2bBhB8EiwAZUbP1BAKPGDQc2Wa5u9WQhwauWc9zelyr52d_Oea7znSUDKj20jfEatsKhoCNQsQAvD_BwE";
	const truemedsLink = "https://www.truemeds.in/";
	const oneMgLink = "https://www.1mg.com/";





	return (
		<ScreenLayout
			isHeaderShown={false}
			isShownHeaderLogo={true}
			headerTitle="hello"
			hamburgmenuVisable={true}
			showBackbutton={false}
			headerbackClick={() => { alert("back button clicked") }}
		>
			<View style={ styles.mainContainer}>
				<View style={styles.segmentOne}>
					{webviewType == 'pharmeasy' ?
						<View style={{ flex: 1 }}>
							<WebView
								source={{ uri: pharmeasyLink }}
								startInLoadingState={true}
								javaScriptEnabled={true}
								domStorageEnabled={true}
							/>
							{/* <View style={{ width: '100%', position: 'absolute', top: 0, height: '100%', opacity: 0.7, backgroundColor: '#fff' }}></View> */}
						</View>
						:
						webviewType == 'netmeds' ?
							<WebView
								source={{ uri: netmedsLink }}
								startInLoadingState={true}
								javaScriptEnabled={true}
								domStorageEnabled={true}
							/>
							:
							webviewType == 'apollo' ?
								<WebView
									source={{ uri: apolloLink }}
									startInLoadingState={true}
									javaScriptEnabled={true}
									domStorageEnabled={true}
								/>
								:
								webviewType == 'truemeds' ?
									<WebView
										source={{ uri: truemedsLink }}
										startInLoadingState={true}
										javaScriptEnabled={true}
										domStorageEnabled={true}
									/>
									:

									webviewType == 'oneMg' ?
										<WebView
											source={{ uri: oneMgLink }}
											startInLoadingState={true}
											javaScriptEnabled={true}
											domStorageEnabled={true}
										/>
										:
										<></>
					}


				</View>
				<View
					style={styles.segmentTwo}>
					{/* <ScrollView horizontal={true} style={{width:'100%',alignSelf:'center'}}> */}
					<View style={styles.segTwoMain}>
						<TouchableOpacity
							onPress={() => { setWebviewType('apollo') }}
							style={{
								height: 55, width: 55, borderRadius: 10, marginRight: 10,

								borderWidth: webviewType == 'apollo' ? 1 : 0,
								borderColor: '#1d2d53',
								padding: webviewType == 'apollo' ? 10 : 0
							}}>
							<Image source={Images.apollo_icon} style={{ height: '100%', width: '100%', resizeMode: 'cover', borderRadius: 10 }} />

						</TouchableOpacity>


						<TouchableOpacity
							onPress={() => { setWebviewType('pharmeasy') }}
							style={{
								height: 55, width: 55, borderRadius: 10, marginRight: 10,

								borderWidth: webviewType == 'pharmeasy' ? 1 : 0,
								borderColor: '#1d2d53',
								padding: webviewType == 'pharmeasy' ? 10 : 0
							}}>
							<Image source={Images.pharmeasy_icon} style={{ height: '100%', width: '100%', resizeMode: 'cover', borderRadius: 10 }} />
						</TouchableOpacity>


						<TouchableOpacity
							onPress={() => { setWebviewType('netmeds') }}
							style={{
								// height:55,width:55,borderRadius:10,
								height: 55, width: 55, borderRadius: 10,
								// marginLeft: 10,
								marginRight: 10,

								borderWidth: webviewType == 'netmeds' ? 1 : 0,
								borderColor: '#1d2d53',
								padding: webviewType == 'netmeds' ? 10 : 0
							}}>
							<Image source={Images.netmed_icon} style={{ height: '100%', width: '100%', resizeMode: 'cover', borderRadius: 10 }} />
						</TouchableOpacity>


						<TouchableOpacity
							onPress={() => { setWebviewType('truemeds') }}
							style={{
								height: 55, width: 55, borderRadius: 10, marginRight: 10,

								borderWidth: webviewType == 'truemeds' ? 1 : 0,
								borderColor: '#1d2d53',
								padding: webviewType == 'truemeds' ? 10 : 0
							}}>
							<Image source={Images.truemeds_icon} style={{ height: '100%', width: '100%', resizeMode: 'cover', borderRadius: 10 }} />
						</TouchableOpacity>

						<TouchableOpacity
							onPress={() => { setWebviewType('oneMg') }}
							style={{
								// height: 55, width: 55, borderRadius: 10,
								height: 55, width: 55, borderRadius: 10,

								borderWidth: webviewType == 'oneMg' ? 1 : 0,
								borderColor: '#1d2d53',
								padding: webviewType == 'oneMg' ? 10 : 0
							}}>
							<Image source={Images.onemg_icon} style={{ height: '100%', width: '100%', resizeMode: 'cover', borderRadius: 10 }} />
						</TouchableOpacity>


					</View>
					{/* </ScrollView> */}
				</View>

			</View>

		</ScreenLayout>
	);
};

//make this component available to the app
export default MedTabScreen;
