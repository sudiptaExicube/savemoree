//import liraries
import React, { Component, useState, useEffect } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Alert,
  BackHandler,
  Dimensions,
  Platform,
  TextInput,
  Linking
} from 'react-native';
import { DrawerActions } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';

import { ScreenLayout } from '../../Components';

import Styles from './Style';
import { useTheme } from '../../Constants/Theme/Theme';
import { Icons, Images } from '../../Constants/ImageIconContant';
import { ColorSpace } from 'react-native-reanimated';

import { useDispatch, useSelector } from 'react-redux';

import MapView, { Marker, Polyline } from 'react-native-maps';
import GetLocation from 'react-native-get-location'
import Icon from 'react-native-vector-icons/Ionicons';

import DeviceInfo from 'react-native-device-info';
import IntentLauncher, { IntentConstant } from 'react-native-intent-launcher'
import { postWithToken } from '../../Service/service';

import MapViewDirections from 'react-native-maps-directions';
import { reducer_setPickup, reducer_setPickupAddress } from '../../Store/Reducers/LocationReducer';
import Geocoder from 'react-native-geocoding';
import { clearUserData } from '../../Store/Reducers/CommonReducer';
import { deleteData, showToastMsg } from '../../Service/localStorage';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
// create a component
const Dashboard = props => {

  const packages = DeviceInfo.getBundleId();

  const styles = Styles();
  const dispatch = useDispatch();
  const { colorTheme } = useTheme();

  const { baseUrl, tempEmailorMobile, userData, userToken, mapApiKey } = useSelector(state => state.common);
  const [pickupLoc, setPickupLoc] = React.useState('');
  const [dropLoc, setDropLoc] = React.useState('');

  const [disableCurrentLocBtn, setDisableCurrentLocBtn] = React.useState(false);

  const { red_pickupLoc, pickupAddress, dropAddress, red_dropLoc } = useSelector(state => state.location);

  const [locationdetails, setLocation] = React.useState({
    latitude: '',
    longitude: ''
  });

  Geocoder.init(mapApiKey);

  // Open app settings
  const openAppSettings = () => {
    if (Platform.OS === 'ios') {
      Linking.openURL('app-settings:')
    } else {
      IntentLauncher.startActivity({
        action: 'android.settings.APPLICATION_DETAILS_SETTINGS',
        data: 'package:' + packages
      })
    }
  }

  const openSettingAlert = () =>
    Alert.alert(
      "Location",
      "Please go to app settings and give location permission",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "Open setting",
          onPress: () => {
            openAppSettings()
          }
        }
      ]
    );

  const checkApi = () => {
    console.log("userToken : ", userToken)
    postWithToken(baseUrl, 'service_list', {}, userToken)
      .then((response) => {
        if(response.success){
          console.log("else success d : ", response.data.food)

        }else{
          if(response.code == 401){
            showConfirmDialog("Logout", "Token is expired. please logout and sign in again","Logout", "LOGOUT")
          }else{
            showToastMsg(response.message)
          }
        }
      })
      .catch((error) => {
        console.log("Error : ", error)
      })
  }

  	/* == Show logout confirmation alert ==*/
	const showConfirmDialog = (title, body, actionText, type) => {
		return Alert.alert(
			title,
			body,
			[
				{
					text: actionText, onPress: () => {
						type == 'LOGOUT' ? singOutFunc() : BackHandler.exitApp();
					},
				}
			]
		);
	};

  /*== Logout functionality start === */
  const singOutFunc = () => {
    console.log("signout : ");
    dispatch(clearUserData())
    deleteData();
    props.navigation.replace('LoginScreen');
    if(userData.login_type == 3){
      GoogleSignin.signOut();
    }
  }

  /* == Use focus effect == */
	useFocusEffect(
		React.useCallback(() => {
      if (
        red_pickupLoc.latitude != "" && red_pickupLoc.longitude != "" &&
        red_pickupLoc.latitude != undefined && red_pickupLoc.longitude != undefined
      ) {
        console.log("red_pickupLoc : ", red_pickupLoc)
        setLocation({
          latitude: red_pickupLoc.latitude,
          longitude: red_pickupLoc.longitude
        })
      } else {
        // Fetch current location
        fetchGeoLocation()
      }
		}, [])
	);

/* == useEffect == */
  useEffect(() => {
    // checkApi()
  /*
    if (
      red_pickupLoc.latitude != "" && red_pickupLoc.longitude != "" &&
      red_pickupLoc.latitude != undefined && red_pickupLoc.longitude != undefined
    ) {
      console.log("red_pickupLoc : ", red_pickupLoc)
      setLocation({
        latitude: red_pickupLoc.latitude,
        longitude: red_pickupLoc.longitude
      })
    } else {
      // Fetch current location
      fetchGeoLocation()
    }
    */
  }, [])

  /* == Fetch current location == */
  const fetchGeoLocation = () => {
    console.log("red_pickupLoc not found ....")
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    })
      .then(locationRes => {
        console.log("locationRes :", locationRes);
        if (locationRes) {
          setDisableCurrentLocBtn(false)
          setLocation({
            latitude: locationRes.latitude,
            longitude: locationRes.longitude
          })
          let geo_loc = {
            lat: locationRes.latitude,
            lng: locationRes.longitude
          }
          dispatch(reducer_setPickup(geo_loc))
          // Fetch address from coords
          startReverseGeoCoding(locationRes.latitude,locationRes.longitude)
        }
      })

      .catch(error => {
        const { code, message } = error;
        if (code == "UNAVAILABLE") {
          alert("Please turn on your device location")
        } else if (code == "UNAUTHORIZED") {
          // alert("Please go to app settings and give location permission")
          openSettingAlert()
        }
        else {
          alert(message)
        }

        console.log(code, message);
      })
  }

  /* == Reverse geocoding is for find full address using coords ==*/
  const startReverseGeoCoding = (latitude,longitude) => {
    Geocoder.from(latitude, longitude)
      .then(json => {
        if (json) {
          
          let addressComponent = json.results[0].formatted_address;
          dispatch(reducer_setPickupAddress(addressComponent))
        }
      })
      .catch(error => {
        console.log("csdcsjkdcskdjcsdkjccsdlcnsdkjcdjkscksdj")
        console.warn("cdscsd:",error)
      });
  }

  const fetchCurrentLoc = () =>{
    setDisableCurrentLocBtn(true)
    fetchGeoLocation()
  }


  return (
    <ScreenLayout
      isHeaderShown={true}
      isShownHeaderLogo={true}
      headerTitle="hello"
      hamburgmenuVisable={true}
      showBackbutton={false}
      headerbackClick={() => { alert("back button clicked") }}
      linerBackground={['#1c263f', '#1c263f']}
    >
      <View style={styles.mainContainer}>
        <View style={styles.sectionOne}></View>
        <View style={styles.sectionTwo}>
          {
            red_pickupLoc.latitude != "" &&
              red_pickupLoc.latitude != "" &&
              red_pickupLoc.latitude != undefined &&
              red_pickupLoc.latitude != undefined
              ?
              <MapView
                style={styles.mapContainer}
                // provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                region={{
                  latitude: red_pickupLoc.latitude,
                  longitude: red_pickupLoc.longitude,
                  latitudeDelta: 0.015,
                  longitudeDelta: 0.0121,
                }}
              >
                {/* Pickup marker */}
                {
                  red_pickupLoc.latitude != "" &&
                    red_pickupLoc.latitude != "" &&
                    red_pickupLoc.latitude != undefined &&
                    red_pickupLoc.latitude != undefined
                    ?
                    <Marker
                      coordinate={{
                        latitude: red_pickupLoc.latitude,
                        longitude: red_pickupLoc.longitude,
                      }}
                      title={"Pickup"}
                      description={"Pickup address"}
                    >
                      <Image
                        source={Icons.pickup_marker_icon}
                        style={{ height: 50, width: 50 }}
                        resizeMode="cover"
                      />
                    </Marker>
                    :
                    null
                }

                {/* Drop off marker */}
                {
                  red_dropLoc.latitude != "" &&
                    red_dropLoc.latitude != "" &&
                    red_dropLoc.latitude != undefined &&
                    red_dropLoc.latitude != undefined
                    ?

                    <Marker
                      coordinate={{
                        latitude: red_dropLoc.latitude,
                        longitude: red_dropLoc.longitude,
                      }}
                      title={"Home"}
                      description={"This is Demo location"}
                    >
                      <Image
                        source={Icons.drop_marker_icon}
                        style={{ height: 50, width: 50 }}
                        resizeMode="contain"
                      />
                    </Marker>
                    :
                    null
                }

                {/* Polyline */}
                {
                  red_pickupLoc.latitude != "" &&
                    red_pickupLoc.longitude != "" &&
                    red_dropLoc.latitude != "" &&
                    red_dropLoc.longitude != "" ?

                    <MapViewDirections
                      origin={red_pickupLoc}
                      destination={red_dropLoc}
                      apikey={'AIzaSyBOqfqXfRYmyF1Hrvqyqc08nzR67m_j8Aw'} // insert your API Key here
                      strokeWidth={3}
                      strokeColor="#1d2d53"
                    />

                    : null
                }

              </MapView>

              : null
          }

        </View>

        <View style={styles.sectionThree}>
          <View style={styles.sectionThreeSubCont}>

            <TouchableOpacity style={styles.rowOne}
              onPress={() => { props.navigation.navigate('LocationScreen', { locType: 'pickup' }) }}
            >
              <View style={styles.imageCont}>
                <Image style={styles.iconImage_custom} source={Icons.pickup_icon} />
              </View>
              <View style={styles.textCont}>
                <Text numberOfLines={1} style={{ paddingLeft: 10, width: '100%' }}>{pickupAddress ? pickupAddress : 'Pickup location'}</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.rowTwo}
              onPress={() => { props.navigation.navigate('LocationScreen', { locType: 'drop' }) }}
            >
              <View style={{ flexDirection: 'column', justifyContent: 'center', width: 18, height: 40 }}>
                <Image source={Icons.drop_icon} style={{ width: '100%', resizeMode: 'contain' }} />
              </View>
              <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', height: 40 }}>
                <Text numberOfLines={1} style={{ paddingLeft: 10, width: '100%' }}>{dropAddress ? dropAddress : 'Destination'}</Text>
              </View>
            </TouchableOpacity>

          </View>
        </View>
        

        <TouchableOpacity 
          onPress={()=>{ disableCurrentLocBtn == false ? fetchCurrentLoc() : showToastMsg("Process in progress. please wait a moment")}}
          style={{position:'absolute',bottom:'25%',backgroundColor:'red',right:30,borderRadius:45,height:45,width:45,backgroundColor:'#FFF',elevation:9,flexDirection:'row',justifyContent:'center'}}>
          <View style={{flexDirection:'column',justifyContent:'center'}}>
            <Icon name="locate-outline"  size={20} color={'#000'} />
          </View>

        </TouchableOpacity>

      </View>
    </ScreenLayout>
  );
};

export default Dashboard;
//props.navigation.navigate('DashDetails')