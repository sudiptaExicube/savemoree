//import liraries
import React, { Component } from 'react';
import { Dimensions, StyleSheet } from 'react-native';
import { FontFamily, FontSize } from '../../Constants/Fonts';
import { useTheme } from '../../Constants/Theme/Theme';
import { windowHeight, windowWidth } from '../../Constants/window';


// create a component
const Styles = () => {
  const { colorTheme } = useTheme();
  return StyleSheet.create({
    mainContainer:{
      flexDirection:'column',justifyContent:'flex-start',width:'100%',height:'100%',flex:1
    },
    sectionOne:{height:60,backgroundColor:'#1c263f'},
    sectionTwo:{flex:1},
    mapContainer:{
      width:'100%',height:Dimensions.get('window').height - 180
    },


    sectionThree:{
      width:'100%',position:'absolute',top:10,paddingLeft:30,paddingRight:30
    },
    sectionThreeSubCont:{
      backgroundColor:'white',padding:10,paddingTop:10,borderRadius:5
    },
    rowOne:{
      flexDirection:'row',justifyContent:'flex-start',backgroundColor:'#efefef',paddingLeft:10,paddingRight:10,borderRadius:5,
    },
    rowTwo:{
      flexDirection:'row',justifyContent:'flex-start',backgroundColor:'#efefef',paddingLeft:10,paddingRight:10,borderRadius:5,marginTop:10
    },
    imageCont:{flexDirection:'column',justifyContent:'center',width:18,height:40},
    iconImage_custom:{width:'100%',resizeMode:'contain'},
    textCont:{flex:1,flexDirection:'column',justifyContent:'center'}


  });
};
export default Styles;
