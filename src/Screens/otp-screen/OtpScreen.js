//import liraries
import React, { createRef, useEffect, useState } from 'react';
import { View, Text, ImageBackground, KeyboardAvoidingView, ScrollView, Image, TextInput, BackHandler } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Styles from './Style';
import { ScreenLayout, SpinnerLoading } from '../../Components';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icons, Images } from '../../Constants/ImageIconContant';
import { useTheme } from '../../Constants/Theme/Theme';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import { useFocusEffect } from '@react-navigation/native';
import { postWithOutToken } from '../../Service/service';
import { setData, showToastMsg } from '../../Service/localStorage';
import { setUserData } from '../../Store/Reducers/CommonReducer';

const OtpScreen = props => {
	const { colorTheme } = useTheme();
	const styles = Styles();
	const dispatch = useDispatch();

	const [pass1, setPass1] = React.useState('');
	const [pass2, setPass2] = React.useState('');
	const [pass3, setPass3] = React.useState('');
	const [pass4, setPass4] = React.useState('');

	const pass1Ref = createRef();
	const pass2Ref = createRef();
	const pass3Ref = createRef();
	const pass4Ref = createRef()

	const { baseUrl, tempEmailorMobile, userData, userToken } = useSelector(state => state.common);



	// const [timerValue, setTimerValue] = React.useState(30);
	const [showTimer, setShowTimer] = React.useState(false);
	const [timerValue, setTimerValue] = React.useState(30);
	let timerId;

	//Spinner
	const [spinnerLoading, setSpinnerLoading] = React.useState(false);
	const [showLoading, setShowLoading] = React.useState(false);

	/* == For hardware back button functionality == */
	useFocusEffect(
		React.useCallback(() => {
			const onBackPress = () => {
				clearInterval(timerId);
				// props.navigation.goBack();
				props.navigation.replace('LoginScreen')
				return true;
			};
			BackHandler.addEventListener('hardwareBackPress', onBackPress);
			return () =>
				BackHandler.removeEventListener('hardwareBackPress', onBackPress);
		}, [])
	);

	React.useEffect(() => {
		if (showTimer) {
			timerId = setInterval(() => {
				if (timerValue < 0) {
					console.log("if : ", timerValue);
					clearInterval(timerId);
				} else if (timerValue == 0) {
					console.log("else if : ", timerValue);
					clearInterval(timerId);
					setShowTimer(false)
					setTimerValue(5)
				} else {
					console.log("timerRef.current : ", timerValue);
					setTimerValue(timerValue - 1);
				}


			}, 1000);
			return () => {
				clearInterval(timerId);
			};
		}
	})


	/* == Submit button function ==*/
	const gotoHomeScreen = () => {
		if (pass1 && pass2 && pass3 && pass4) {
			setShowLoading(true)
			setSpinnerLoading(true)
			let concatData = pass1.toString() + pass2.toString() + pass3.toString() + pass4.toString();
			console.log("concat data : ", concatData);
			let paramData = {
				"email": tempEmailorMobile,
				"code": concatData
			}
			startEmailVerification(paramData)
		} else {
			console.log("not all field fillup");
			console.log("fields : ", pass1, pass2, pass3, pass4);
		}
	}

	/* == Start email Verification ==*/
	const startEmailVerification = (paramData) => {
		console.log("paramdata : ", paramData);
		postWithOutToken(baseUrl, 'email_verification', paramData)
			.then((resp) => {
				console.log("email_verification response success : ", resp)
				showToastMsg(resp.message)
				if (resp.success) {
					dispatch(setUserData(resp));
					setShowLoading(false)
					setSpinnerLoading(false)
					storeDataToLocalstorage(resp);
					props.navigation.replace('Home')

				} else {
					setShowLoading(false)
					setSpinnerLoading(false)
					showToastMsg(resp.message)
				}
			})
			.catch((error) => {
				setShowLoading(false)
				setSpinnerLoading(false)
				console.log("login response error : ", error)
			})
	}

	/* == Resend OTP function ==*/
	const resendOtp = () => {
		setShowLoading(true)
		setSpinnerLoading(true)
		let paramData = {
			"email": tempEmailorMobile
		}
		postWithOutToken(baseUrl, 'resend_otp', paramData)
			.then((resp) => {
				console.log("resend_otp response success : ", resp)
				showToastMsg(resp.message)
				if (resp.success) {
					dispatch(setUserData(resp));
					setShowLoading(false)
					setSpinnerLoading(false)
				} else {
					setShowLoading(false)
					setSpinnerLoading(false)
					showToastMsg(resp.message)
				}
			})
			.catch((error) => {
				setShowLoading(false)
				setSpinnerLoading(false)
				console.log("login response error : ", error)
			})
	}

	/* == Localstorage data add Function == */
	const storeDataToLocalstorage = (response) => {
		setData("userDetails", JSON.stringify(response.data));
		setData("token", response.token);
	}


	return (
		<ScreenLayout
			isHeaderShown={false}
			isShownHeaderLogo={false}
			headerTitle="SignIn">

			<ImageBackground
				source={Images.loginbackground}
				style={styles.backgroundImageView}
			>
				<KeyboardAvoidingView
					behavior="padding"
					keyboardVerticalOffset={35}
					style={styles.customKeyboardAvoidingView}>


					<View style={styles.mainView}>
						<ScrollView showsVerticalScrollIndicator={false} style={styles.customScrollView}>
							<View style={styles.secOne}>
								<Image source={Icons.applogo} />
							</View>
							<View style={styles.secTwo}>
								<Text style={styles.vText}>VERIFICATION!</Text>
							</View>
							<View style={styles.secThree}>
								<Text style={styles.smalltext}>You will get a OTP via Email or Number</Text>
							</View>


							<View style={styles.secFour}>
								{showTimer ?
									<Text style={styles.otpText}>OTP expired in {timerValue} sec</Text>
									: <></>
								}

							</View>

							<View style={styles.secFive}>
								<View style={styles.textRowMainView}>
									<View style={styles.textboxMainDiv}>
										<TextInput
											keyboardType='numeric'
											value={pass1}
											maxLength={1}
											onChangeText={text => {
												console.log("text : ", text)
												setPass1(text);
												if (text) { this.pass2Ref.focus() }
												else { }
											}}
											// placeholder={'Name'}
											underlineColorAndroid={colorTheme.white_color}
											color={colorTheme.white_color}
											returnKeyType='next'
											style={{ backgroundColor: 'white', borderRadius: 10, textAlign: 'center', height: 50, color: '#bcbcbc', fontSize: 22 }}

											ref={pass1Ref => this.pass1Ref = pass1Ref}
											onSubmitEditing={() => this.pass2Ref.focus()}
										>
										</TextInput>
									</View>
								</View>

								<View style={styles.textRowMainView}>
									<View style={styles.textboxMainDiv}>
										<TextInput
											keyboardType='numeric'
											value={pass2}
											maxLength={1}
											onChangeText={text => {
												setPass2(text)
												if (text) { this.pass3Ref.focus() }
												else { this.pass1Ref.focus() }
											}}
											// placeholder={'Email or Mobile Number'}
											underlineColorAndroid={colorTheme.white_color}
											color={colorTheme.white_color}
											returnKeyType='next'
											style={{ backgroundColor: 'white', borderRadius: 10, textAlign: 'center', height: 50, color: '#bcbcbc', fontSize: 22 }}

											ref={pass2Ref => this.pass2Ref = pass2Ref}
											onSubmitEditing={() => this.pass3Ref.focus()}
										>
										</TextInput>
									</View>
								</View>

								<View style={styles.textRowMainView}>
									<View style={styles.textboxMainDiv}>
										<TextInput
											keyboardType='numeric'
											value={pass3}
											maxLength={1}
											onChangeText={text => {
												setPass3(text)
												if (text) { this.pass4Ref.focus() }
												else { this.pass2Ref.focus() }
											}}
											// placeholder={'Password'}
											underlineColorAndroid={colorTheme.white_color}
											color={colorTheme.white_color}
											returnKeyType='next'
											style={{ backgroundColor: 'white', borderRadius: 10, textAlign: 'center', height: 50, color: '#bcbcbc', fontSize: 22 }}

											ref={pass3Ref => this.pass3Ref = pass3Ref}
											onSubmitEditing={() => this.pass4Ref.focus()}
										>
										</TextInput>
									</View>
								</View>

								<View style={styles.textRowMainView}>
									<View style={styles.textboxMainDiv}>
										<TextInput
											keyboardType='numeric'
											value={pass4}
											maxLength={1}
											onChangeText={text => {
												setPass4(text)

												setTimeout(() => {
													if (text) {
														// gotoHomeScreen()
													}
													else {
														this.pass3Ref.focus();
													}
												}, 100);
											}}
											// placeholder={'Password'}
											underlineColorAndroid={colorTheme.white_color}
											color={colorTheme.white_color}
											returnKeyType='done'
											style={{ backgroundColor: 'white', borderRadius: 10, textAlign: 'center', height: 50, color: '#bcbcbc', fontSize: 22 }}

											ref={pass4Ref => this.pass4Ref = pass4Ref}
											onSubmitEditing={() => { gotoHomeScreen() }}
											onChange={(e) => {
												console.log("e : ", e)
											}}
										>
										</TextInput>
									</View>
								</View>
							</View>

							<View style={styles.secSix}>
								{/* <Text style={{ fontSize: 14, color: 'white', letterSpacing: 0.3 }}>Enter your OTP</Text> */}
								{showTimer == true ? <></> :
									<TouchableOpacity onPress={() => {
										clearInterval(timerId);
										setShowTimer(true);
										resendOtp();
									}}>
										<Text style={styles.resendText}> Resend OTP</Text>
									</TouchableOpacity>
								}
							</View>



							<View style={styles.inputcontainerView}>
								<View style={styles.btnMainDiv}>
									{/* {showTimer == true ? <></>: */}
									<TouchableOpacity
										// onPress={() => { clearInterval(timerId);  setShowTimer(true) }}
										onPress={() => { gotoHomeScreen() }}
									>

										<View style={styles.btnCntner}>
											<LinearGradient colors={['#4d5361', '#a1a4ac', '#fff']} style={styles.linBtn}>
												<Text style={styles.submitText}>Submit</Text>
											</LinearGradient>
										</View>
									</TouchableOpacity>
									{/* } */}
								</View>
							</View>
						</ScrollView>


					</View>

					{showLoading ? <SpinnerLoading showSpinner={spinnerLoading} /> : <></>}
				</KeyboardAvoidingView>
			</ImageBackground>
		</ScreenLayout>
	);
};

//make this component available to the app
export default OtpScreen;
