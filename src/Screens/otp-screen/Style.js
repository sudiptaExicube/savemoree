//import liraries
import React, {Component} from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import { FontFamily, FontSize } from '../../Constants/Fonts';
import { useTheme } from '../../Constants/Theme/Theme';


// create a component
const Styles = () => {
  const {colorTheme} = useTheme();
  return StyleSheet.create({
    customScrollView:{ height:'100%', },
    backgroundImageView:{width: '100%', height: '100%'},
    customKeyboardAvoidingView:{ height:'100%'}, 

    mainView:{flex:1, flexDirection:'column',padding:20,paddingTop:150},

    textRowMainView:{flexDirection:'row',justifyContent:'center',marginTop:20,marginLeft:10},
    inputcontainerView:{flexDirection:'row',justifyContent:'center',marginTop:30},
    textboxMainDiv:{width:50},
    btnMainDiv:{width:200},
    btnCntner:{ height: 40, borderRadius: 20, flexDirection: 'column', justifyContent: 'center' },
    linBtn:{ height: 40, borderRadius: 20, flexDirection: 'column', justifyContent: 'center' },
    submitText:{ textAlign: 'center', fontSize: 18, letterSpacing: 0.2, color: '#fff' },

    secOne:{ flexDirection: 'row', justifyContent: 'center' },
    secTwo:{ marginTop: 50, flexDirection: 'row', justifyContent: 'center' },
    vText:{ fontSize: 25, color: 'white', fontWeight: '600', letterSpacing: 0.4 },
    
    secThree:{ flexDirection: 'row', justifyContent: 'center' },
    smalltext:{ fontSize: 14, color: 'white', letterSpacing: 0.3 },

    secFour:{ flexDirection: 'row', justifyContent: 'center', marginTop: 50 },
    otpText:{ fontSize: 14, color: 'white', letterSpacing: 0.3 },

    secFive:{ flexDirection: 'row', justifyContent: 'center' },

    secSix:{
      flexDirection: 'row',
      // justifyContent: 'center',
      justifyContent: 'flex-end', marginRight: '20%',
      marginTop: 20
    },
    resendText:{ color: 'white', fontWeight: '600', letterSpacing: 0.5, paddingLeft: 20 },

  });
};
export default Styles;