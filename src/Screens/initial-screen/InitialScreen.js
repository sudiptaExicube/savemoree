//import liraries
import React, { useEffect } from 'react';
import { View, Text, StyleSheet, ImageBackground, Image, Dimensions } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useTheme } from '../../Constants/Theme/Theme';

import { ScreenLayout } from '../../Components';


import Styles from './Style';
import { Icons } from '../../Constants/ImageIconContant';
import RNBootSplash from 'react-native-bootsplash';
import { clearUserData, localstorage_TokenAdd, localstorage_UserdetailsAdd } from '../../Store/Reducers/CommonReducer';
import { getData } from '../../Service/localStorage';



// create a component
const Initial = props => {
	const { colorTheme } = useTheme();
	const styles = Styles()
	const dispatch = useDispatch();
	const { userData,userToken } = useSelector(state => state.common);


	useEffect(() => {
		// setTimeout(() => {
		// 	// props.navigation.replace('Home');
		// 	offSplashScreen();
		// 	props.navigation.replace('LoginScreen');
		// }, 1000);

	    console.log("useeffect calling")
	    setTimeout(() => {
	      getData('token').then((tokenSuccess)=>{
	        if(tokenSuccess){
	          dispatch(localstorage_TokenAdd(tokenSuccess));

						getData('userDetails').then((respSuccess)=>{
						if(respSuccess){
							dispatch(localstorage_UserdetailsAdd(JSON.parse(respSuccess)))
							offSplashScreen();
							props.navigation.replace('Home');
						}else{
							dispatch(clearUserData())
							offSplashScreen();
							props.navigation.replace('LoginScreen');
						}
						})

	        }else{
	          dispatch(clearUserData())
	          offSplashScreen();
	          props.navigation.replace('LoginScreen');
	        }

	      })      

	    }, 200);

	  }, []);

	  const offSplashScreen = () => {
	    console.log("settimeout calling")
	    setTimeout(() => {
	      RNBootSplash.hide()
	    }, 1000);
	  }

	return (

		<ScreenLayout
			isHeaderShown={false}
			isShownHeaderLogo={true}
			headerTitle="hello"
			hamburgmenuVisable={false}
			showBackbutton={false}
			headerbackClick={() => { alert("back button clicked") }}
		>
			<View style={styles.mainContainer}>
				<View style={styles.innerContainer}>
					<Image source={Icons.applogo} style={styles.customImage} />
				</View>
			</View>
		</ScreenLayout>
	);
};


//make this component available to the app
export default Initial;
