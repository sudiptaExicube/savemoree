//import liraries
import React, {Component} from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import { FontFamily, FontSize } from '../../Constants/Fonts';
import { useTheme } from '../../Constants/Theme/Theme';


// create a component
const Styles = () => {
  const {colorTheme} = useTheme();
  return StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      // backgroundColor: colorTheme.backGroundColor,
    },
    backgroundImageView:{width: Dimensions.get('window').width, height: Dimensions.get('window').height,resizeMode:'contain'},


  mainContainer:{backgroundColor:colorTheme.primaryColor,height:'100%',width:'100%',
    flex:1,flexDirection:'column',justifyContent:'center'
  },
  innerContainer:{flexDirection:'row',justifyContent:'center',width:'100%',height:'20%'},
  customImage:{ height: '30%', width: '50%' }






  });
};
export default Styles;