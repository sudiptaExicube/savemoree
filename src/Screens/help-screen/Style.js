//import liraries
import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { FontFamily, FontSize } from '../../Constants/Fonts';
import { useTheme } from '../../Constants/Theme/Theme';
import { windowHeight, windowWidth } from '../../Constants/window';


// create a component
const Styles = () => {
  const { colorTheme } = useTheme();
  return StyleSheet.create({
    mainContainer:{
      flex: 1, height: '100%', backgroundColor: 'white', paddingTop: 10, paddingBottom: '20%'
    },
    subContainer:{
      paddingTop: 10, flexDirection: 'column', justifyContent: 'center', flex: 1, backgroundColor: '#FFF', flex: 1, height: '100%'
    },
    collapseHeaderContainer:{
      flexDirection: 'row', justifyContent: 'space-between', paddingLeft: 10, paddingRight: 10, backgroundColor: '#F0EEEE', margin: 20, marginBottom: 5, marginTop: 0, borderRadius: 5
    },
    titleContainer:{
      flexDirection: 'column', justifyContent: 'center'
    },
    textTitle:{
      fontSize: 16, fontWidth: 600, letterSpacing: 0.6
    },
    iconView:{
      paddingTop: 10, paddingBottom: 10
    },
    bodymainView:{
      flexDirection: 'row', justifyContent: 'space-between', paddingLeft: 10, paddingRight: 10, margin: 20, marginBottom: 5, marginTop: 0 
    },
    bodyText:{
      lineHeight: 17, letterSpacing: 0.6, fontSize: 14
    }


    
  });
};
export default Styles;
