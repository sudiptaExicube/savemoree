//import liraries
import React, { Component, useState, useEffect } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Alert,
  BackHandler,
  Dimensions,
  Platform,
  TextInput
} from 'react-native';
import { DrawerActions } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';

import { ScreenLayout } from '../../Components';

import Styles from './Style';
import { useTheme } from '../../Constants/Theme/Theme';
import { useDispatch, useSelector } from 'react-redux';

import { Collapse, CollapseHeader, CollapseBody, AccordionList } from 'accordion-collapse-react-native';
import { FlatList } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';

const demoData = [
  {
    'title': 'What is Lorem Ipsum?',
    'value': "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
  }, {
    'title': 'Why do we use it?',
    'value': "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
  }, {
    'title': 'Where does it come from?',
    'value': "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
  }, {
    'title': 'Where can I get some?',
    'value': "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
  }
]

// create a component
const HelpScreen = props => {
  const styles = Styles();
  const dispatch = useDispatch();
  const { colorTheme } = useTheme();

  const { baseUrl, tempEmailorMobile, userData, userToken } = useSelector(state => state.common);

  const [selectIndex, setSelectndex] = React.useState(null)
  const [list, setList] = React.useState(demoData);

  useEffect(() => { }, [])

  /* == For hardware back button functionality == */
	useFocusEffect(
		React.useCallback(() => {
			const onBackPress = () => {
				// props.navigation.goBack();
				props.navigation.replace('Home')
				return true;
			};
			BackHandler.addEventListener('hardwareBackPress', onBackPress);
			return () =>
				BackHandler.removeEventListener('hardwareBackPress', onBackPress);
		}, [])
	);

  const renderItem = ({ item, index }) => (
    <Collapse onToggle={(isExpanded) => {
      if (isExpanded) { setSelectndex(index) } else { setSelectndex(null) }
    }}
    >
      {/* F0EEEE */}
      <CollapseHeader>
        <View style={styles.collapseHeaderContainer}>
          <View style={styles.titleContainer}>
            <Text style={styles.textTitle}>
              {item?.title}
            </Text>
          </View>
          <View style={styles.iconView}>
            <Icon name={index == selectIndex ? 'chevron-down-outline' : 'chevron-forward-outline'} size={18} />
          </View>

        </View>
      </CollapseHeader>
      <CollapseBody
        style={styles.bodymainView}
      >
        <Text style={styles.bodyText}>{item?.value}</Text>
      </CollapseBody>
    </Collapse>
  );

  return (
    <ScreenLayout
      isHeaderShown={true}
      isShownHeaderLogo={false}
      headerTitle="Help"
      hamburgmenuVisable={false}
      showBackbutton={true}
      headerbackClick={() => {
        props.navigation.replace('Home')
      }}
      linerBackground={['#1c263f', '#1c263f']}
    >
      <View style={styles.mainContainer}
      >
        <View
          style={styles.subContainer}
        >
          <FlatList
            data={list}
            renderItem={renderItem}
            keyExtractor={item => item.title}
          />
        </View>
      </View>

    </ScreenLayout>
  );
};

export default HelpScreen;
//props.navigation.navigate('DashDetails')