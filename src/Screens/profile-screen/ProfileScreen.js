//import liraries
import React, { Component, useState, useEffect } from 'react';
import {
	View,
	Text,
	Image,
	StyleSheet,
	TouchableOpacity,
	Alert,
	BackHandler,
	Dimensions
} from 'react-native';
import { DrawerActions } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';

import { ScreenLayout } from '../../Components';

import Styles from './Style';
import { useTheme } from '../../Constants/Theme/Theme';
import { Icons, Images } from '../../Constants/ImageIconContant';
import { ColorSpace } from 'react-native-reanimated';

import { useDispatch, useSelector } from 'react-redux';

import MapView from 'react-native-maps';
import Icon from 'react-native-vector-icons/Ionicons';
import { ScrollView } from 'react-native-gesture-handler';

import { clearUserData } from '../../Store/Reducers/CommonReducer';
import { deleteData } from '../../Service/localStorage';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
// create a component
const ProfileScreen = props => {
	const styles = Styles();
	const dispatch = useDispatch();
	const { userData, userToken } = useSelector(state => state.common);

	/* == For hardware back button functionality == */
	useFocusEffect(
		React.useCallback(() => {
			const onBackPress = () => {
				// props.navigation.goBack();
				props.navigation.replace('Home')
				return true;
			};
			BackHandler.addEventListener('hardwareBackPress', onBackPress);
			return () =>
				BackHandler.removeEventListener('hardwareBackPress', onBackPress);
		}, [])
	);



	useEffect(() => {
	}, [])

	/* == Show logout confirmation alert ==*/
	const showConfirmDialog = (title, body, actionText, type) => {
		return Alert.alert(
			title,
			body,
			[
				{
					text: actionText, onPress: () => {
						type == 'LOGOUT' ? singOutFunc() : BackHandler.exitApp();
					},
				},
				{
					text: "Cancel",
				},
			]
		);
	};

	/*== Logout functionality start === */
	const singOutFunc = () => {
		console.log("signout : ");
		dispatch(clearUserData())
		deleteData();
		props.navigation.replace('LoginScreen');
		if (userData.login_type == 3) {
			GoogleSignin.signOut();
		}
	}

	/*== Logout Button click function === */
	const singOutBtn = () => {
		showConfirmDialog("Logout", "Are you sure you want to logout from this app?", "Logout", "LOGOUT")
	}



	return (
		<ScreenLayout
			isHeaderShown={true}
			isShownHeaderLogo={true}
			headerTitle=""
			hamburgmenuVisable={false}
			showBackbutton={true}
			headerbackClick={() => {
				// props.navigation.goBack();
				props.navigation.replace('Home')
				// alert("hello")

			}}
			linerBackground={['#1c263f', '#1c263f']}
		>
			<View style={styles.mainContainer}>
				<View style={styles.space}></View>
				<View style={styles.secOne}>

					<View style={styles.profileMain}>
						<View style={styles.profileSub}>
							<Image source={Images.profile_image} style={styles.proImg} />

							<TouchableOpacity onPress={() => { alert("hello") }} >
								<View style={styles.overlayMain}>
									<View  style={styles.overlaySub}>
										<View style={styles.overlayImgMain}>
											<View style={styles.overlayImgSub}>
												<Image source={Icons.camera_icon} style={styles.overlayImg} />
											</View>
										</View>
									</View>
								</View>
							</TouchableOpacity>

						</View>
					</View>

				</View>

				<View style={styles.nameMain}>
					<Text style={styles.nameText}>{userData?.name ? userData.name : ''}</Text>
				</View>


				<View style={styles.optionMain}>
					<ScrollView style={styles.scrollableView}>

						<TouchableOpacity
							style={styles.touchableMain}>
							<Text style={styles.touchableText}>My Account</Text>
						</TouchableOpacity>

						<TouchableOpacity
							style={styles.touchableMain}>
							<Text style={styles.touchableText}>My Orders</Text>
						</TouchableOpacity>

						<TouchableOpacity
							style={styles.touchableMain}>
							<Text style={styles.touchableText}>Booking History</Text>
						</TouchableOpacity>

						<TouchableOpacity
							onPress={() => { singOutBtn() }}
							style={styles.touchableMain}>
							<Text style={styles.touchableText}>Log out</Text>
						</TouchableOpacity>

					</ScrollView>
				</View>


				<View style={{ flex: 0.5, backgroundColor: '#ececec' }}></View>

			</View>


		</ScreenLayout>
	);
};

export default ProfileScreen;
