//import liraries
import React, {Component} from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import { FontFamily, FontSize } from '../../Constants/Fonts';
import { useTheme } from '../../Constants/Theme/Theme';


// create a component
const Styles = () => {
  const {colorTheme} = useTheme();
  return StyleSheet.create({
    mainContainer:{ flexDirection: 'column', justifyContent: 'flex-start', width: '100%', flex: 1, backgroundColor: '#1c263f' },
    space:{ width: '100%', flex: 0.8, backgroundColor: '#1c263f' },
    secOne:{ width: '100%', flex: 0.8, backgroundColor: '#ececec', borderRadius: 20, borderBottomRightRadius: 0, borderBottomLeftRadius: 0 },
    profileMain:{ height: 130, width: '100%', position: 'absolute', top: -65, flexDirection: 'row', justifyContent: 'center' },
    profileSub:{ height: 130, width: 130, flexDirection: 'row', justifyContent: 'center', borderRadius: 25, borderWidth: 1, borderColor: '#fff' },
    proImg:{ height: 130, width: 130, resizeMode: 'cover', borderRadius: 25 },

    overlayMain:{ height: 40, width: 40, backgroundColor: '#fff', position: 'absolute', bottom: -13, right: -13, borderRadius: 8, flexDirection: 'column', justifyContent: 'center' },
    overlaySub:{ flexDirection: 'row', justifyContent: 'center' },

    overlayImgMain:{ height: 34, width: 34, backgroundColor: '#ececec', borderRadius: 8, flexDirection: 'column', justifyContent: 'center' },
    overlayImgSub:{ flexDirection: 'row', justifyContent: 'center', width: '100%' },
    overlayImg:{ height: 18, width: 18, resizeMode: 'cover' },

    nameMain:{ backgroundColor: '#ececec' },
    nameText:{ textAlign: 'center', fontSize: 20, textTransform: 'capitalize', letterSpacing: 0.4, fontWeight: '600', paddingTop: 20, paddingBottom: 10, color: '#8b8b8b' },


    optionMain:{ flex: 5, backgroundColor: '#ececec', padding: 40, paddingBottom: 0, paddingTop: 0 },
    scrollableView:{ flex: 1 },
    touchableMain:{
      height: 55, backgroundColor: 'white', borderRadius: 10, flexDirection: 'column',
      justifyContent: 'center', paddingLeft: 20, paddingRight: 20, marginTop: 15
    },
    touchableText:{ fontSize: 16, color: '#8b8b8b', fontWeight: '600', letterSpacing: 0.4 }



  });
};
export default Styles;