//import liraries
import React, { Component, useState, useEffect } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Alert,
  BackHandler,
  Dimensions,
  Platform,
  TextInput
} from 'react-native';
import { DrawerActions } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';

import { ScreenLayout } from '../../Components';

import Styles from './Style';
import { useTheme } from '../../Constants/Theme/Theme';
import { useDispatch, useSelector } from 'react-redux';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { ScrollView } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import{reducer_setDrop, reducer_setDropAddress, reducer_setPickup, reducer_setPickupAddress} from '../../Store/Reducers/LocationReducer';

// create a component
const LocationScreen = props => {
  const styles = Styles();
  const dispatch = useDispatch();
  const { colorTheme } = useTheme();

  // params
  const { locType } = props.route.params;

  const { baseUrl,tempEmailorMobile,userData,userToken,mapApiKey } = useSelector(state => state.common);
  const {   red_pickupLoc,pickupAddress  } = useSelector(state => state.location);


  useEffect(() => {
  }, [])


  const setSearchValue = (details) =>{
    // console.log("1 location :", details.geometry.location)
    if(locType == 'pickup'){
      dispatch(reducer_setPickup(details.geometry.location))
      dispatch(reducer_setPickupAddress(details.formatted_address))
      setTimeout(() => {
        props.navigation.goBack();
      }, 1000);

    }else if (locType == 'drop'){

      dispatch(reducer_setDrop(details.geometry.location))
      dispatch(reducer_setDropAddress(details.formatted_address))
      setTimeout(() => {
        props.navigation.goBack();
      }, 1000);
    }
  }

  return (
    <ScreenLayout
      isHeaderShown={true}
			isShownHeaderLogo={false}
			headerTitle="Search address"
			hamburgmenuVisable={false}
			showBackbutton={true}
			headerbackClick={() => { 
				props.navigation.replace('Home')
			}}
      linerBackground={['#1c263f','#1c263f']}
		>
      <View style={styles.mainContainer}>
        <View style={styles.subContainer}>
          <ScrollView>
            <GooglePlacesAutocomplete
              keyboardShouldPersistTaps='handled'
              listViewDisplayed={false}
              keepResultsAfterBlur={true}
              placeholder='Search location'
              fetchDetails={true}
              onPress={(data, details = null) => {
                setSearchValue(details)
              }}
              query={{
                // key: 'AIzaSyBOqfqXfRYmyF1Hrvqyqc08nzR67m_j8Aw',
                key:mapApiKey,
                language: 'en',
                components:"country:in"
              }}
            />
            </ScrollView>

        </View>


      </View>
		</ScreenLayout>
  );
};

export default LocationScreen;
//props.navigation.navigate('DashDetails')