//import liraries
import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { FontFamily, FontSize } from '../../Constants/Fonts';
import { useTheme } from '../../Constants/Theme/Theme';
import { windowHeight, windowWidth } from '../../Constants/window';


// create a component
const Styles = () => {
  const { colorTheme } = useTheme();
  return StyleSheet.create({
    mainContainer:{
      flexDirection:'column',justifyContent:'flex-start',
      width:'100%',height:'100%',flex:1,paddingLeft:10,paddingRight:10
    },
    subContainer:{
      paddingLeft:10,paddingRight:10,marginTop:20
    },




  });
};
export default Styles;
