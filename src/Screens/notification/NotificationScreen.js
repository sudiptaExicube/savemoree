//import liraries
import React, { Component, useState, useEffect } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Alert,
  BackHandler,
  Dimensions,
  Platform,
  TextInput
} from 'react-native';
import { DrawerActions } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';

import { ScreenLayout } from '../../Components';

import Styles from './Style';
import { useTheme } from '../../Constants/Theme/Theme';
import { useDispatch, useSelector } from 'react-redux';

import {Collapse,CollapseHeader, CollapseBody, AccordionList} from 'accordion-collapse-react-native';
import { FlatList } from 'react-native-gesture-handler';
import  Icon from 'react-native-vector-icons/Ionicons';


// create a component
const NotificationScreen = props => {
  const styles = Styles();
  const dispatch = useDispatch();
  const { colorTheme } = useTheme();

  const { baseUrl,tempEmailorMobile,userData,userToken } = useSelector(state => state.common);
  
  const [selectIndex, setSelectndex] = React.useState(null)
  



  useEffect(() => {}, [])

  /* == For hardware back button functionality == */
	useFocusEffect(
		React.useCallback(() => {
			const onBackPress = () => {
				// props.navigation.goBack();
				props.navigation.replace('Home')
				return true;
			};
			BackHandler.addEventListener('hardwareBackPress', onBackPress);
			return () =>
				BackHandler.removeEventListener('hardwareBackPress', onBackPress);
		}, [])
	);  


  return (
    <ScreenLayout
      isHeaderShown={true}
			isShownHeaderLogo={true}
			headerTitle=""
			hamburgmenuVisable={false}
			showBackbutton={true}
			headerbackClick={() => { 
				props.navigation.replace('Home')
			}}
      linerBackground={['#1c263f','#1c263f']}
		>
      <View style={{flexDirection:'column',justifyContent:'center',width:'100%',height:'100%',flex:1}}>
        <Text style={{textAlign:'center',fontSize:20,letterSpacing:0.5,color:'black'}}>COMING SOON</Text>
      </View>


		</ScreenLayout>
  );
};

export default NotificationScreen;
//props.navigation.navigate('DashDetails')