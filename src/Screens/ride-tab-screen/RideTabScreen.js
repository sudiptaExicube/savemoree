
//import liraries
import React, { createRef, useEffect, useRef, useState } from 'react';
import { View, Text, Image, Dimensions } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Styles from './Style';
// import { useTheme } from '../../../Constants/Theme/Theme';
import { useTheme } from '../../Constants/Theme/Theme';
import { ScreenLayout } from '../../Components';
import { Icons, Images } from '../../Constants/ImageIconContant';
import { WebView } from 'react-native-webview';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import { useFocusEffect } from '@react-navigation/native';

const RideTabScreen = props => {
	// const { colorTheme } = useTheme();
	const styles = Styles();
	const dispatch = useDispatch();
	const [webviewType, setWebviewType] = React.useState('merucab');
	// const olaLink = "https://book.olacabs.com/";
	// const olaLink = "http://book.olacabs.com/?lat=12.935&lng=77.614&category=compact&utm_source=12343&drop_lat=12.979&drop_lng=77.590&dsw=yes";

	// const uberLink = "https://m.uber.com";
	// const uberLink = "https://m.uber.com/?action=setPickup&pickup[latitude]=37.775818&pickup[longitude]=-122.418028&pickup[nickname]=UberHQ&pickup[formatted_address]=1455%20Market%20St%2C%20San%20Francisco%2C%20CA%2094103&dropoff[latitude]=37.802374&dropoff[longitude]=-122.405818&dropoff[nickname]=Coit%20Tower&dropoff[formatted_address]=1%20Telegraph%20Hill%20Blvd%2C%20San%20Francisco%2C%20CA%2094133&product_id=a1111c8c-c720-46c3-8534-2fcdd730040d"
	// const uberLink = "https://m.uber.com/ul/?client_id=1xKqvYWk-t3Q6eamQbQn142NTs97KEzY"
	const indriverLink = "https://indriver.com/";
	const merucabLink = "https://www.meru.in/local";

	const [olaLink, setOlaLink] = React.useState('https://book.olacabs.com/');
	const [uberLink, setUberLink] = React.useState('https://m.uber.com');


  const { red_pickupLoc, pickupAddress, dropAddress, red_dropLoc } = useSelector(state => state.location);

	useFocusEffect(
		React.useCallback(() => {
			console.log("ride ..... ")
			// setTimeout(() => {
			// 	setWebviewType('merucab')
			// }, 100);

			if(
				red_pickupLoc.latitude != '' && 
				red_pickupLoc.longitude != '' &&
				red_pickupLoc.latitude != undefined && 
				red_pickupLoc.longitude != undefined &&
	
				red_dropLoc.latitude != '' && 
				red_dropLoc.longitude != '' &&
				red_dropLoc.latitude != undefined && 
				red_dropLoc.longitude != undefined 
			)
			{
				console.log("all pickup and drop location found ........ ")
				let demoOlalink = `https://book.olacabs.com/?lat=${red_pickupLoc.latitude}&lng=${red_pickupLoc.longitude}&category=compact&utm_source=12343&drop_lat=${red_dropLoc.latitude}&drop_lng=${red_dropLoc.longitude}&dsw=yes`;
				setOlaLink(demoOlalink);
			}else{
				console.log("/........all pickup and drop location not founc ")
				setOlaLink('https://book.olacabs.com/')
			}


		}, [])
	);

	useEffect(() => {
		
		// setTimeout(() => {
		// 	setWebviewType('merucab')
		// }, 2000);
		
		// if(
		// 	red_pickupLoc.latitude != '' && 
		// 	red_pickupLoc.longitude != '' &&
		// 	red_pickupLoc.latitude != undefined && 
		// 	red_pickupLoc.longitude != undefined &&

		// 	red_dropLoc.latitude != '' && 
		// 	red_dropLoc.longitude != '' &&
		// 	red_dropLoc.latitude != undefined && 
		// 	red_dropLoc.longitude != undefined 
		// )
		// {
		// 	let demoOlalink = `https://book.olacabs.com/?lat=${red_pickupLoc.latitude}&lng=${red_pickupLoc.longitude}&category=compact&utm_source=12343&drop_lat=${red_dropLoc.latitude}&drop_lng=${red_dropLoc.longitude}&dsw=yes`;
		// 	// let demoUberlink=`https://m.uber.com/ul/?client_id=1xKqvYWk-t3Q6eamQbQn142NTs97KEzY&action=setPickup&pickup[latitude]=${red_pickupLoc.latitude}&pickup[longitude]=-${red_pickupLoc.longitude}&pickup[nickname]=UberHQ&pickup[formatted_address]=1455%20Market%20St%2C%20San%20Francisco%2C%20CA%2094103&dropoff[latitude]=${red_dropLoc.latitude}&dropoff[longitude]=${red_dropLoc.longitude}&dropoff[nickname]=Coit%20Tower&dropoff[formatted_address]=1%20Telegraph%20Hill%20Blvd%2C%20San%20Francisco%2C%20CA%2094133&product_id=a1111c8c-c720-46c3-8534-2fcdd730040d`
		// 	setOlaLink(demoOlalink);
		// 	// setUberLink(demoUberlink);
		// }else{
		// 	setOlaLink('https://book.olacabs.com/')
		// 	// setUberLink('https://m.uber.com');
		// }
	},[])

	const webViewRef = useRef();
	return (
		<ScreenLayout
			isHeaderShown={false}
			isShownHeaderLogo={true}
			headerTitle="hello"
			hamburgmenuVisable={true}
			showBackbutton={false}
			headerbackClick={() => { alert("back button clicked") }}
		>
			<View style={{
				// flex:1, 
				height: Dimensions.get('window').height - 60,
				// marginBottom:60,
				backgroundColor: '#ececec'
			}}>
				<View style={{ flex: 1 }}>
					{webviewType == 'ola' ?
						// <View style={{flex:1}}>
						<WebView
							style={{ flex: 1 }}
							source={{ uri: olaLink }}
							startInLoadingState={true}
							javaScriptEnabled={true}
							domStorageEnabled={true}
						/>

						// {/* </View> */}

						:
						webviewType == 'uber' ?
							<WebView
								style={{ flex: 1 }}
								nestedScrollEnabled={true}
								source={{ uri: uberLink }}
								startInLoadingState={true}
								javaScriptEnabled={true}
								domStorageEnabled={true}
							/>
							:
							// webviewType == 'indriver'?
							// 	<WebView 
							// 		style={{flex:1}}
							// 		source={{ uri: indriverLink }} 
							// 		startInLoadingState={true} 
							// 		javaScriptEnabled = {true}
							//     domStorageEnabled = {true}
							// 	/>
							// :
							webviewType == 'merucab' ?
								<WebView
									style={{ flex: 1 }}
									source={{ uri: merucabLink }}
									startInLoadingState={true}
									javaScriptEnabled={true}
									domStorageEnabled={true}
								/>
								:
								<></>
					}




				</View>
				<View
					style={{
						height: 80, backgroundColor: '#fff', flexDirection: 'column',
						justifyContent: 'center',
						// marginLeft:10,marginRight:10,
						paddingLeft: 10, paddingRight: 10,
						// borderTopLeftRadius:15,borderTopRightRadius:15
					}}>
					<View style={{ flexDirection: 'row', justifyContent: 'center' }}>
						<TouchableOpacity
							onPress={() => { setWebviewType('merucab') }}
							style={{
								height: 55, width: 55, borderRadius: 10,

								borderWidth: webviewType == 'merucab' ? 1 : 0,
								borderColor: '#1d2d53',
								padding: webviewType == 'merucab' ? 10 : 0
							}}>
							<Image source={Images.merucab_icon} style={{ height: '100%', width: '100%', resizeMode: 'cover', borderRadius: 10 }} />
						</TouchableOpacity>

						<TouchableOpacity
							onPress={() => { setWebviewType('uber') }}
							style={{
								height: 55, width: 55, borderRadius: 10, marginLeft: 15, marginRight: 15,

								borderWidth: webviewType == 'uber' ? 1 : 0,
								borderColor: '#1d2d53',
								padding: webviewType == 'uber' ? 10 : 0
							}}>
							<Image source={Images.uber_ride_icon} style={{ height: '100%', width: '100%', resizeMode: 'cover', borderRadius: 10 }} />
						</TouchableOpacity>

						<TouchableOpacity
							onPress={() => {
								setWebviewType('ola')
							}}
							style={{
								height: 55, width: 55, borderRadius: 10,

								borderWidth: webviewType == 'ola' ? 1 : 0,
								borderColor: '#1d2d53',
								padding: webviewType == 'ola' ? 10 : 0
							}}>
							<Image source={Images.ola_ride_icon}
								style={{
									height: '100%', width: '100%', resizeMode: 'cover', borderRadius: 10,

								}} />
						</TouchableOpacity>
					</View>
				</View>

			</View>

		</ScreenLayout>
	);
};

//make this component available to the app
export default RideTabScreen;
