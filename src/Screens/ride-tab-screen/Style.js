//import liraries
import React, {Component} from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import { FontFamily, FontSize } from '../../Constants/Fonts';
import { useTheme } from '../../Constants/Theme/Theme';


// create a component
const Styles = () => {
  const {colorTheme} = useTheme();
  return StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      // backgroundColor: colorTheme.backGroundColor,
    },
    backgroundImageView:{width: Dimensions.get('window').width, height: Dimensions.get('window').height,resizeMode:'contain'},
  });
};
export default Styles;