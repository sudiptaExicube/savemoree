//import liraries
import React, { createRef, useEffect, useState } from 'react';
import { View, Text, ImageBackground, KeyboardAvoidingView, ScrollView, Image, TextInput, ActivityIndicator } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Styles from './Style';
import { ScreenLayout, SpinnerLoading } from '../../Components';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icons, Images } from '../../Constants/ImageIconContant';
import { useTheme } from '../../Constants/Theme/Theme';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import { LoginButton, AccessToken, GraphRequest, GraphRequestManager, Settings } from 'react-native-fbsdk-next';
import { LoginManager } from "react-native-fbsdk-next";
import { Profile } from "react-native-fbsdk-next";
import { saveData, setTempEmail, setUserData } from '../../Store/Reducers/CommonReducer';
import { setData, showToastMsg } from '../../Service/localStorage';
import { postWithOutToken } from '../../Service/service';


import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin';

const Login = props => {
	const { colorTheme } = useTheme();
	const styles = Styles();
	const dispatch = useDispatch();
  // const [emailorMobile, setEmailorMobile] = React.useState('sayansanu30@gmail.com');
  const [emailorMobile, setEmailorMobile] = React.useState('');
  const [password, setPassword] = React.useState('');

  //Spinner
  const [spinnerLoading, setSpinnerLoading] = React.useState(false);
  const [showLoading, setShowLoading] = React.useState(false);

  // const { userData } = useSelector(state => state.common);
  const { baseUrl,tempEmailorMobile } = useSelector(state => state.common);


  // ==> Button Focas reference create
  const emailorMobileRef = createRef()
  const passwordCodeRef = createRef()

  // {"email": "mondal.pradip647@gmail.com", "first_name": "Pradip", "id": "5922292454514386", "last_name": "Mondal", "name": "Pradip Mondal"}

  const clickFacebookLogin =() =>{
    Settings.setAppID('2908889906079288');
    Settings.initializeSDK();
        LoginManager.setLoginBehavior("web_only");
        LoginManager.logInWithPermissions(["public_profile","email"]).then(
          function(result) {
            if (result.isCancelled) {
              console.log("Login cancelled");
            } else {
              const infoRequest = new GraphRequest(
                '/me?fields=email,name,first_name,last_name',
                null,
                (err, res) => {
                  if(res){
                    console.log("response : ", res);
                    let paramData = {
                      "email":res.email,
                      "name":res.name,
                      "unique_id":res.id,
                      "login_type":2
                    }
                    setShowLoading(true)
                    setSpinnerLoading(true)
                    socialLoginData_StoredInDatabase(paramData);
                  }
                  if(err){
                    console.log("infoRequest error ")
                  }
                })
                new GraphRequestManager().addRequest(infoRequest).start();
            }
          },
          function(error) {
            console.log("Login fail with error: " + error);
          }
        );
  }

  /* == Localstorage data add Function == */
	const storeDataToLocalstorage = (response) => {
		setData("userDetails",JSON.stringify(response.data));
		setData("token",response.token);
	}

  /* == Facebook data stored in the Database == */
  const socialLoginData_StoredInDatabase=(paramData)=>{
    postWithOutToken(baseUrl, 'login', paramData)
      .then((resp) => {
        console.log("social login response success : ", resp)
        showToastMsg(resp.message)
        if (resp.success) {
          // dispatch(setTempEmail(emailorMobile))
          setShowLoading(false)
          setSpinnerLoading(false)

					dispatch(setUserData(resp));
					storeDataToLocalstorage(resp);
					props.navigation.replace('Home')	
        } else {
          setShowLoading(false)
          setSpinnerLoading(false)
          showToastMsg(resp.message)
        }
      })
      .catch((error) => {
        setShowLoading(false)
        setSpinnerLoading(false)
        console.log("login response error : ", error)
      })
  }


  /* == Login button function == */
  const loginFuncClick = ()=>{
    if(!emailorMobile){
      showToastMsg("Email or Mobile number can not be empty")
    }
    // else if (!password){
    //   showToastMsg("Password field can not be empty")
    // }
    else{
      let paramdata={
        // "email":"sayansanu30@gmail.com",
        "email":emailorMobile,
        "login_type":1
    }
        setShowLoading(true)
        setSpinnerLoading(true)
        // setTimeout(() => {
          startLogin(paramdata)
        // }, 2000);
    }
    console.log("base URL :", baseUrl);
  }

  /* == login API call Function == */
  const startLogin = (paramData) => {
    postWithOutToken(baseUrl, 'login', paramData)
    // postWithOutToken(baseUrl, 'PayrollApiUrl', paramData)
      .then((resp) => {
        console.log("Login response success : ", resp)
        showToastMsg(resp.message)
        if (resp.success) {
          dispatch(setTempEmail(emailorMobile))
          setShowLoading(false)
          setSpinnerLoading(false)
          setTimeout(() => {
            props.navigation.replace('OtpScreen')
            // console.log("tempEmailorMobile : ", tempEmailorMobile);
          }, 1000);

          
        } else {
          setShowLoading(false)
          setSpinnerLoading(false)
          showToastMsg(resp.message)
        }
      })
      .catch((error) => {
        setShowLoading(false)
        setSpinnerLoading(false)
        console.log("login response error : ", error)
      })
  }

  useEffect(()=>{
  })

  // Somewhere in your code
  signInWithGoogle = async () => {
    try {
      GoogleSignin.configure(
      {
        offlineAccess: true,
        webClientId:'548219107986-gh5l6mgtf38402gfsmg4c35lle336p2t.apps.googleusercontent.com',
        androidClientId: '548219107986-chbgqfeoh8rmhi7v9ft3limo42ai2klf.apps.googleusercontent.com',
        scopes: ['profile', 'email']
      });
      await GoogleSignin.hasPlayServices();
      console.log("reached google sign in");
      const userInfo = await GoogleSignin.signIn();
      console.log(JSON.stringify(userInfo));
      if(userInfo){
        // console.log("google : ", JSON.stringify(userInfo));
        let paramData = {
          "email":userInfo.user.email,
          "name":userInfo.user.name,
          "unique_id":userInfo.user.id,
          "login_type":3
        }  
        setShowLoading(true)
        setSpinnerLoading(true)
        socialLoginData_StoredInDatabase(paramData);
      }

    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log("error occured SIGN_IN_CANCELLED");
        if(error.message){
          showToastMsg(JSON.stringify(error.message))
        }
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log("error occured IN_PROGRESS");
        // showToastMsg(JSON.stringify(error))
        if(error.message){
          showToastMsg(JSON.stringify(error.message))
        }
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log("error occured PLAY_SERVICES_NOT_AVAILABLE");
        // showToastMsg(JSON.stringify(error))
        if(error.message){
          showToastMsg(JSON.stringify(error.message))
        }
      } else {
        console.log(error)
        // showToastMsg(JSON.stringify(error))
        console.log("error occured unknow error");
        if(error.message){
          showToastMsg(JSON.stringify(error.message))
        }
      }
    }

  };



	return (
    <ScreenLayout
      isHeaderShown={false}
      isShownHeaderLogo={false}
      headerTitle="SignIn">
        
      <ImageBackground
        source={Images.loginbackground}
        style={styles.backgroundImageView}
      >
        <KeyboardAvoidingView
          behavior="padding"
          keyboardVerticalOffset={35}
          style={styles.customKeyboardAvoidingView}>

            <View style={styles.mainView}>          
              <ScrollView showsVerticalScrollIndicator={false} style={styles.customScrollView}>

              <View style={{flexDirection:'row',justifyContent:'center'}}>
                <Image source={Icons.applogo} />
              </View>
              <View style={{marginTop:50,flexDirection:'row',justifyContent:'center'}}>
                <Text style={{fontSize:40,color:'white',fontWeight:'600',letterSpacing: 0.4}}>WELCOME!</Text>
              </View>
              <View style={{flexDirection:'row',justifyContent:'center'}}>
                <Text style={{fontSize:16,color:'white',letterSpacing: 0.4}}>Signin to continue</Text>
              </View>

              <View style={styles.inputcontainerView}>
                <View style={styles.textboxMainDiv}>
                  <TextInput
                    keyboardType='default'
                    value={emailorMobile}
                    onChangeText={text => setEmailorMobile(text)}
                    placeholder={'Email or Mobile Number'}
                    underlineColorAndroid={colorTheme.white_color}
                    color={colorTheme.white_color}
                    returnKeyType='next'
                    style={{backgroundColor:'white',borderRadius:10,textAlign:'center',height:50,color:'#bcbcbc'}}

                    ref={emailorMobileRef => this.emailorMobileRef = emailorMobileRef}
                    onSubmitEditing={() => this.passwordCodeRef.focus()}
                  >
                    </TextInput>
                </View>
              </View>

              {/* <View style={styles.inputcontainerView}>
                <View style={styles.textboxMainDiv}>
                  <TextInput
                    keyboardType='default'
                    value={password}
                    onChangeText={text => setPassword(text)}
                    placeholder={'Password'}
                    underlineColorAndroid={colorTheme.white_color}
                    color={colorTheme.white_color}
                    returnKeyType='done'
                    style={{backgroundColor:'white',borderRadius:10,textAlign:'center',height:50,color:'#bcbcbc'}}

                    ref={passwordCodeRef => this.passwordCodeRef = passwordCodeRef}
                    onSubmitEditing={() => {}}
                  >
                    </TextInput>
                </View>
              </View> */}

              <View style={styles.inputcontainerView}>
                <View style={styles.btnMainDiv}>
                    <TouchableOpacity  onPress={()=>{
                      // props.navigation.replace("OtpScreen")
                        loginFuncClick()
                      }}>
                      
                        <View style={{height:40,borderRadius:20,flexDirection:'column',justifyContent:'center'}}>
                          <LinearGradient colors={[ '#4d5361','#a1a4ac','#fff']} style={{height:40,borderRadius:20,flexDirection:'column',justifyContent:'center'}}>
                            <Text style={{textAlign:'center',fontSize:18,letterSpacing:0.2,color:'#fff'}}>Log in</Text>
                          </LinearGradient>
                        </View>
                    </TouchableOpacity>
                </View>
              </View>
              <TouchableOpacity 
                onPress={()=>{alert("forgot password clicked")}}
                style={{flexDirection:'row',justifyContent:'center',marginTop:15}}>
                <Text style={{fontSize:14,color:'white',letterSpacing:1}}>Forgot Password?</Text>
              </TouchableOpacity>

              <View>
                <View style={{flexDirection:'row',justifyContent:'center'}}>
                  <View style={{flexDirection:'row',justifyContent:'center',width:'80%'}}>
                    <View style={{flex:1,height:1,marginTop:'5%',borderTopWidth:1,borderTopColor:'#fff',borderStyle:'solid'}}></View>
                    <View><Text style={{fontSize:20,color:'white',paddingLeft:5,paddingRight:5}}>or</Text></View>
                    <View style={{flex:1,height:1,marginTop:'5%',borderTopWidth:1,borderTopColor:'#fff',borderStyle:'solid'}}></View>
                  </View>
                </View>
              </View>

              <View style={{flexDirection:'row',justifyContent:'center',marginTop:20}}>
                <Text style={{fontSize:14,color:'white',letterSpacing:1}}>Social Media Login</Text>
              </View>
              <View style={{flexDirection:'row',justifyContent:'center',marginTop:15}}>
                  <View style={{height:50,width:50,borderRadius:50,backgroundColor:'#fff',marginLeft:10,marginRight:10,flexDirection:'column',justifyContent:'center'}}>
                    <TouchableOpacity 
                      onPress={()=>{clickFacebookLogin()}}
                      style={{flexDirection:'row',justifyContent:'center'}}>
                      <Image source={Icons.facebook_icon} style={{height:40,resizeMode:'contain'}} />
                    </TouchableOpacity>
                  </View>

                  <View style={{height:50,width:50,borderRadius:50,backgroundColor:'#fff',marginLeft:10,marginRight:10,flexDirection:'column',justifyContent:'center'}}>
                    <TouchableOpacity 
                      onPress={()=>{
                        // alert("Google login click")
                        signInWithGoogle()
                      }}
                      style={{flexDirection:'row',justifyContent:'center'}}>
                      <Image source={Icons.google_icon} style={{height:40,resizeMode:'contain'}} />
                    </TouchableOpacity>
                  </View>
              </View>

              <View style={{flexDirection:'row',justifyContent:'center',marginTop:20}}>
                <Text style={{fontSize:14,color:'white',letterSpacing:1}}>Don’t have an account?</Text>
              </View>
              <TouchableOpacity 
                onPress={()=>{props.navigation.replace('SignupScreen')}}
                style={{flexDirection:'row',justifyContent:'center',marginTop:2}}>
                <Text style={{fontSize:16,color:'white',letterSpacing:1,fontWeight:'600'}}> Sign up</Text>
              </TouchableOpacity>

              </ScrollView>
            </View>
            {showLoading ? <SpinnerLoading showSpinner={spinnerLoading} />:<></>}
            


        </KeyboardAvoidingView>
      </ImageBackground>
    </ScreenLayout>
	);
};

//make this component available to the app
export default Login;



// 548219107986-bpmh7fk3gbfmgpjng5bslg7q5287hkps.apps.googleusercontent.com