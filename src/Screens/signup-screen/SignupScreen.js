//import liraries
import React, { createRef, useEffect, useState } from 'react';
import { View, Text, ImageBackground, KeyboardAvoidingView, ScrollView, Image, TextInput, BackHandler } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Styles from './Style';
import { ScreenLayout } from '../../Components';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icons, Images } from '../../Constants/ImageIconContant';
import { useTheme } from '../../Constants/Theme/Theme';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import { useFocusEffect } from '@react-navigation/native';

const Signup = props => {
	const { colorTheme } = useTheme();
	const styles = Styles();
	const dispatch = useDispatch();
	const [name, setName] = React.useState('');
	const [emailorMobile, setEmailorMobile] = React.useState('');
	const [password, setPassword] = React.useState('');

	// ==> Button Focas reference create
	const nameRef = createRef()
	const emailorMobileRef = createRef()
	const passwordCodeRef = createRef()

  /* == For hardware back button functionality == */
  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
				// props.navigation.goBack();
				props.navigation.replace('LoginScreen')
				return true;
      };
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [])
  );


	return (
		<ScreenLayout
			isHeaderShown={false}
			isShownHeaderLogo={false}
			headerTitle="SignIn">

			<ImageBackground
				source={Images.loginbackground}
				style={styles.backgroundImageView}
			>
				<KeyboardAvoidingView
					behavior="padding"
					keyboardVerticalOffset={35}
					style={styles.customKeyboardAvoidingView}>
					<ScrollView showsVerticalScrollIndicator={false} style={styles.customScrollView}>

						<View style={styles.mainView}>
							<View style={{ flexDirection: 'row', justifyContent: 'center' }}>
								<Image source={Icons.applogo} />
							</View>
							<View style={{ marginTop: 50, flexDirection: 'row', justifyContent: 'center' }}>
								<Text style={{ fontSize: 40, color: 'white', fontWeight: '600', letterSpacing: 0.4 }}>HI!</Text>
							</View>
							<View style={{ flexDirection: 'row', justifyContent: 'center' }}>
								<Text style={{ fontSize: 16, color: 'white', letterSpacing: 0.4 }}>Create a new account</Text>
							</View>

							<View style={styles.inputcontainerView}>
								<View style={styles.textboxMainDiv}>
									<TextInput
										keyboardType='default'
										value={name}
										onChangeText={text => setName(text)}
										placeholder={'Name'}
										underlineColorAndroid={colorTheme.white_color}
										color={colorTheme.white_color}
										returnKeyType='next'
										style={{ backgroundColor: 'white', borderRadius: 10, textAlign: 'center', height: 50, color: '#bcbcbc' }}

										ref={nameRef => this.nameRef = nameRef}
										onSubmitEditing={() => this.emailorMobileRef.focus()}
									>
									</TextInput>
								</View>
							</View>

							<View style={styles.inputcontainerView}>
								<View style={styles.textboxMainDiv}>
									<TextInput
										keyboardType='default'
										value={emailorMobile}
										onChangeText={text => setEmailorMobile(text)}
										placeholder={'Email or Mobile Number'}
										underlineColorAndroid={colorTheme.white_color}
										color={colorTheme.white_color}
										returnKeyType='next'
										style={{ backgroundColor: 'white', borderRadius: 10, textAlign: 'center', height: 50, color: '#bcbcbc' }}

										ref={emailorMobileRef => this.emailorMobileRef = emailorMobileRef}
										onSubmitEditing={() => this.passwordCodeRef.focus()}
									>
									</TextInput>
								</View>
							</View>

							<View style={styles.inputcontainerView}>
								<View style={styles.textboxMainDiv}>
									<TextInput
										keyboardType='default'
										value={password}
										onChangeText={text => setPassword(text)}
										placeholder={'Password'}
										underlineColorAndroid={colorTheme.white_color}
										color={colorTheme.white_color}
										returnKeyType='done'
										style={{ backgroundColor: 'white', borderRadius: 10, textAlign: 'center', height: 50, color: '#bcbcbc' }}

										ref={passwordCodeRef => this.passwordCodeRef = passwordCodeRef}
										onSubmitEditing={() => { }}
									>
									</TextInput>
								</View>
							</View>

							<View style={styles.inputcontainerView}>
								<View style={styles.btnMainDiv}>
									<TouchableOpacity onPress={() => { alert("submit button working") }}>

										<View style={{ height: 40, borderRadius: 20, flexDirection: 'column', justifyContent: 'center' }}>
											<LinearGradient colors={['#4d5361', '#a1a4ac', '#fff']} style={{ height: 40, borderRadius: 20, flexDirection: 'column', justifyContent: 'center' }}>
												<Text style={{ textAlign: 'center', fontSize: 18, letterSpacing: 0.2, color: '#fff' }}>Sign up</Text>
											</LinearGradient>
										</View>
									</TouchableOpacity>
								</View>
							</View>

							<View style={{marginTop:20}}>
								<View style={{ flexDirection: 'row', justifyContent: 'center' }}>
									<View style={{ flexDirection: 'row', justifyContent: 'center', width: '80%' }}>
										<View style={{ flex: 1, height: 1, marginTop: '5%', borderTopWidth: 1, borderTopColor: '#fff', borderStyle: 'solid' }}></View>
										<View><Text style={{ fontSize: 20, color: 'white', paddingLeft: 5, paddingRight: 5 }}>or</Text></View>
										<View style={{ flex: 1, height: 1, marginTop: '5%', borderTopWidth: 1, borderTopColor: '#fff', borderStyle: 'solid' }}></View>
									</View>
								</View>
							</View>

							<View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 20 }}>
								<Text style={{ fontSize: 14, color: 'white', letterSpacing: 1 }}>Social Media Login</Text>
							</View>
							<View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 15 }}>
								<View style={{ height: 50, width: 50, borderRadius: 50, backgroundColor: '#fff', marginLeft: 10, marginRight: 10, flexDirection: 'column', justifyContent: 'center' }}>
									<TouchableOpacity
										onPress={() => { alert("facebook login click") }}
										style={{ flexDirection: 'row', justifyContent: 'center' }}>
										<Image source={Icons.facebook_icon} style={{ height: 40, resizeMode: 'contain' }} />
									</TouchableOpacity>
								</View>

								<View style={{ height: 50, width: 50, borderRadius: 50, backgroundColor: '#fff', marginLeft: 10, marginRight: 10, flexDirection: 'column', justifyContent: 'center' }}>
									<TouchableOpacity
										onPress={() => { alert("Google login click") }}
										style={{ flexDirection: 'row', justifyContent: 'center' }}>
										<Image source={Icons.google_icon} style={{ height: 40, resizeMode: 'contain' }} />
									</TouchableOpacity>
								</View>
							</View>

							<View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 20 }}>
								<Text style={{ fontSize: 14, color: 'white', letterSpacing: 1 }}>Don’t have an account?</Text>
							</View>
							<TouchableOpacity
								onPress={() => { props.navigation.goBack() }}
								style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 2 }}>
								<Text style={{ fontSize: 16, color: 'white', letterSpacing: 1, fontWeight: '600' }}> Sign in</Text>
							</TouchableOpacity>

						</View>

					</ScrollView>
				</KeyboardAvoidingView>
			</ImageBackground>
		</ScreenLayout>
	);
};

//make this component available to the app
export default Signup;
