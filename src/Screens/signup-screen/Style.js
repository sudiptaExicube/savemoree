//import liraries
import React, {Component} from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import { FontFamily, FontSize } from '../../Constants/Fonts';
import { useTheme } from '../../Constants/Theme/Theme';


// create a component
const Styles = () => {
  const {colorTheme} = useTheme();
  return StyleSheet.create({
    customScrollView:{ height:'100%', },
    backgroundImageView:{width: '100%', height: '100%'},
    customKeyboardAvoidingView:{ height:'100%'}, 

    mainView:{flex:1, flexDirection:'column',padding:20,paddingTop:150},

    inputcontainerView:{flexDirection:'row',justifyContent:'center',marginTop:20},
    textboxMainDiv:{width:280},
    btnMainDiv:{width:200},

  });
};
export default Styles;