import AsyncStorage from '@react-native-async-storage/async-storage';
import { ToastAndroid } from 'react-native';

const getData = async key => {
  let value = await AsyncStorage.getItem(key);
  return value;
};

const setData = async (key, value) => {
  let resp = await AsyncStorage.setItem(key, value);
};

const deleteData = async () => {
  let resp = await AsyncStorage.clear();
};

/* == Show Toast msg function == */
const showToastMsg = async (msg) => {
  ToastAndroid.show(msg, ToastAndroid.SHORT)
}

export {getData, setData, deleteData,showToastMsg};
