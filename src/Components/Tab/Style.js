//import liraries
import React, {Component} from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import { FontFamily, FontSize } from '../../Constants/Fonts';
import { useTheme } from '../../Constants/Theme/Theme';
import { windowHeight,windowWidth } from '../../Constants/window';


// create a component
const Styles = () => {
  const {colorTheme} = useTheme();
  return StyleSheet.create({
    container: {
        flexDirection: 'row',
        // justifyContent: 'space-between',
        justifyContent:'center',
        // paddingHorizontal: '15%',
        paddingHorizontal: 15,
        backgroundColor:'#fff'
    },
  });
};
export default Styles;
