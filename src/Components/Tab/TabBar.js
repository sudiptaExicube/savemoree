import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import Styles from './Style';
import { useSelector } from 'react-redux';
import { useTheme } from '@react-navigation/native';

import Icon from 'react-native-vector-icons/Ionicons';

const TabBar = ({ state, descriptors, navigation }) => {
  const [activeRoute, setActiveRoute] = useState('Hometab');
  const { colorTheme } = useTheme();
  const styles = Styles();

  //   const {token, userData} = useSelector(state => state.common);

  useEffect(() => {
    // console.log(SIZES.width)
    console.log("state.routes[state.index].name : ", state.routes[state.index].name);
    setActiveRoute(state.routes[state.index].name);
  }, [state.index]);

  // useEffect(() => {
  // }, []);

  const getRouteKey = name =>
    state.routes.filter(route => route.name === name)[0].key;

  const onNavigate = name => {
    const routeKey = getRouteKey(name);

    // console.log('name : '.routeKey);
    const event = navigation.emit({
      type: 'tabPress',
      target: routeKey,
      canPreventDefault: true,
    });
    if (!(activeRoute === name) && !event.defaultPrevented) {
      navigation.navigate(name);
    }
  };





  return (
    <View style={{ flexDirection: 'column', justifyContent: 'center', height: 60, backgroundColor: '#fff' }}>
      <View style={styles.container}>

        <TouchableOpacity
          style={{flex:1,flexDirection:'row',justifyContent:'center'}}
          accessibilityRole="button"
          accessibilityStates={
          activeRoute === 'Hometab' ? ['selected'] : []
          }
          onPress={() => onNavigate('Hometab')}>
          {activeRoute === 'Hometab'?
          <View style={{flexDirection:'row',justifyContent:'center'}}>
            <View style={{backgroundColor:'#1d2d53',height:40,borderRadius:8,flexDirection:'column',justifyContent:'center',paddingLeft:10,paddingRight:10,minWidth:80,maxWidth:80}}>
              <View style={{flexDirection:'row',justifyContent:'center'}}>
                <View style={{flexDirection:'column',justifyContent:'center'}}>  
                  <Icon name="ios-home" size={16} color={'#fff'} />
                </View>
                <View style={{flexDirection:'column',justifyContent:'center',paddingLeft:8}}>  
                  <Text style={{fontSize:14,color:'white',fontWeight:'600'}}>Home</Text>
                </View>
                
              </View>
            </View>
          </View>
          :
          <View style={{flexDirection:'row',justifyContent:'center'}}>
            <View style={{backgroundColor:'#e5e5e5',height:40,borderRadius:8,flexDirection:'column',justifyContent:'center',paddingLeft:10,paddingRight:10,minWidth:80,maxWidth:80}}>
              <View style={{flexDirection:'row',justifyContent:'center'}}>
                <View style={{flexDirection:'column',justifyContent:'center'}}>  
                  <Icon name="home-outline" size={16} color={'#989898'} />
                </View>
                <View style={{flexDirection:'column',justifyContent:'center',paddingLeft:8}}>  
                  <Text style={{fontSize:14,color:'#989898'}}>Home</Text>
                </View>
                
              </View>
            </View>
          </View>

          }
          </TouchableOpacity>


          <TouchableOpacity
          style={{flex:1,flexDirection:'row',justifyContent:'center',marginLeft:15,marginRight:15}}
          accessibilityRole="button"
          accessibilityStates={
          activeRoute === 'RideTabScreen' ? ['selected'] : []
          }
          onPress={() => onNavigate('RideTabScreen')}>
          {activeRoute === 'RideTabScreen'?
          <View style={{flexDirection:'row',justifyContent:'center'}}>
          <View style={{backgroundColor:'#1d2d53',height:40,borderRadius:8,flexDirection:'column',justifyContent:'center',paddingLeft:10,paddingRight:10,minWidth:80,maxWidth:80}}>
            <View style={{flexDirection:'row',justifyContent:'center'}}>
              <View style={{flexDirection:'column',justifyContent:'center'}}>  
                <Icon name="ios-car-sport" size={16} color={'#fff'} />
              </View>
              <View style={{flexDirection:'column',justifyContent:'center',paddingLeft:8}}>  
                <Text style={{fontSize:14,color:'white',fontWeight:'600'}}>Ride</Text>
              </View>
              
            </View>
          </View>
          </View>

          :
          <View style={{flexDirection:'row',justifyContent:'center'}}>
          <View style={{backgroundColor:'#e5e5e5',height:40,borderRadius:8,flexDirection:'column',justifyContent:'center',paddingLeft:10,paddingRight:10,minWidth:80,maxWidth:80}}>
            <View style={{flexDirection:'row',justifyContent:'center'}}>
              <View style={{flexDirection:'column',justifyContent:'center'}}>  
                <Icon name="ios-car-sport-outline" size={16} color={'#989898'} />
              </View>
              <View style={{flexDirection:'column',justifyContent:'center',paddingLeft:8}}>  
                <Text style={{fontSize:14,color:'#989898'}}>Ride</Text>
              </View>
              
            </View>
          </View>
          </View>

          }
          </TouchableOpacity>


          <TouchableOpacity
          style={{flex:1,flexDirection:'row',justifyContent:'center',marginRight:15}}
          accessibilityRole="button"
          accessibilityStates={
          activeRoute === 'FoodTabScreen' ? ['selected'] : []
          }
          onPress={() => onNavigate('FoodTabScreen')}>
          {activeRoute === 'FoodTabScreen'?
          <View style={{flexDirection:'row',justifyContent:'center'}}>
          <View style={{backgroundColor:'#1d2d53',height:40,borderRadius:8,flexDirection:'column',justifyContent:'center',paddingLeft:10,paddingRight:10,minWidth:80,maxWidth:80}}>
            <View style={{flexDirection:'row',justifyContent:'center'}}>
              <View style={{flexDirection:'column',justifyContent:'center'}}>  
                <Icon name="ios-fast-food" size={16} color={'#fff'} />
              </View>
              <View style={{flexDirection:'column',justifyContent:'center',paddingLeft:8}}>  
                <Text style={{fontSize:14,color:'white',fontWeight:'600'}}>Food</Text>
              </View>
              
            </View>
          </View>
          </View>

          :
          <View style={{flexDirection:'row',justifyContent:'center'}}>
            <View style={{backgroundColor:'#e5e5e5',height:40,borderRadius:8,flexDirection:'column',justifyContent:'center',paddingLeft:10,paddingRight:10,minWidth:80,maxWidth:80}}>
              <View style={{flexDirection:'row',justifyContent:'center'}}>
                <View style={{flexDirection:'column',justifyContent:'center'}}>  
                  <Icon name="ios-fast-food-outline" size={16} color={'#989898'} />
                </View>
                <View style={{flexDirection:'column',justifyContent:'center',paddingLeft:8}}>  
                  <Text style={{fontSize:14,color:'#989898'}}>Food</Text>
                </View>
                
              </View>
            </View>
          </View>

          }
          </TouchableOpacity>


          {/* Medicine tab */}
          <TouchableOpacity
          style={{flex:1,flexDirection:'row',justifyContent:'center',}}
          accessibilityRole="button"
          accessibilityStates={
          activeRoute === 'MedTabScreen' ? ['selected'] : []
          }
          onPress={() => onNavigate('MedTabScreen')}>
          {activeRoute === 'MedTabScreen'?
          <View style={{flexDirection:'row',justifyContent:'center'}}>
          <View style={{backgroundColor:'#1d2d53',height:40,borderRadius:8,flexDirection:'column',justifyContent:'center',paddingLeft:10,paddingRight:10,minWidth:80,maxHeight:80}}>
            <View style={{flexDirection:'row',justifyContent:'center'}}>
              <View style={{flexDirection:'column',justifyContent:'center'}}>  
                <Icon name="medkit" size={16} color={'#fff'} />
              </View>
              <View style={{flexDirection:'column',justifyContent:'center',paddingLeft:8}}>  
                <Text style={{fontSize:14,color:'white',fontWeight:'600'}}>Medicine</Text>
              </View>
              
            </View>
          </View>
          </View>

          :
          <View style={{flexDirection:'row',justifyContent:'center'}}>
          <View style={{backgroundColor:'#e5e5e5',height:40,borderRadius:8,flexDirection:'column',justifyContent:'center',paddingLeft:10,paddingRight:10,minWidth:80,maxHeight:80}}>
            <View style={{flexDirection:'row',justifyContent:'center'}}>
              <View style={{flexDirection:'column',justifyContent:'center'}}>  
                <Icon name="medkit-outline" size={16} color={'#989898'} />
              </View>
              <View style={{flexDirection:'column',justifyContent:'center',paddingLeft:8}}>  
                <Text style={{fontSize:13,color:'#989898'}}>Medicine</Text>
              </View>
              
            </View>
          </View>
          </View>

          }
          </TouchableOpacity>
          
      </View>
    </View>
  );
};

export default TabBar;
