//import liraries
import React, {Component} from 'react';
import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
import {useTheme} from '../../Constants/Theme/Theme';
import {windowHeight} from '../../Constants/window';
import Header from '../Header/Header';
import Styles from './Style';

// create a component
const ScreenLayout = ({ linerBackground,isHeaderShown,isShownHeaderLogo,headerTitle, children, headerbackClick,headermenuClick,showpowerButton,clickPowerbutton,hamburgmenuVisable,showBackbutton}) => {
  const {colorTheme} = useTheme();
  const styles = Styles();
  return (
    <SafeAreaView style={{backgroundColor: colorTheme.backGroundColor}}>
      <View style={styles.container}>
        {isHeaderShown ? (
          <View style={{height: 60, width: '100%'}}>
            <Header isShownHeaderLogo={isShownHeaderLogo} 
            headerTitle={headerTitle} 
            onPress={headerbackClick}
            onPressHeader={headermenuClick}
            showPowerButton={showpowerButton}
            powerbuttonClick={clickPowerbutton}
            ismenuShow={hamburgmenuVisable}
            isbackButton={showBackbutton}
            linerBackground={linerBackground}
            
            />
          </View>
        ) : (
          <></>
        )}
        <View
          style={{
            height: isHeaderShown ? windowHeight : windowHeight,
            width: '100%',
            backgroundColor: colorTheme.backGroundColor,
          }}>
          {children}
        </View>
      </View>
    </SafeAreaView>
  );
};

//make this component available to the app
export default ScreenLayout;


/* === How to use this component ? ===== */
/*
<ScreenLayout
    isHeaderShown={true} // for showing header
    isShownHeaderLogo={false} //for showing header logo
    // ismenuShow={false}  
    headerTitle="Demo title" //fot header title
    headerbackClick={()=>{props.navigation.goBack()}}  // Back button click functionality
    hamburgmenuVisable={true} // for showing hamburg menu. if falase then show back button bu dafault

    // headermenuClick = {()=>{props.navigation.dispatch(DrawerActions.openDrawer())}} // If needed [ bu default hamburg menu click funtionality is opening drawer menu]

    showpowerButton={true} //  For showing right icon, bu default its hidden
    clickPowerbutton ={()=>{alert("hello world")}}   // Right icon functionalities
  >
  </ScreenLayout>
*/

