//import liraries
import {DrawerActions, useNavigation} from '@react-navigation/native';
import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import {useTheme} from '../../Constants/Theme/Theme';
import Styles from './Style';
import Icon from 'react-native-vector-icons/Ionicons';
import {Icons, Images} from '../../Constants/ImageIconContant';
import {FontSize} from '../../Constants/Fonts';
import LinearGradient from 'react-native-linear-gradient';
import { TouchableOpacity } from 'react-native-gesture-handler';

// create a component
const Header = ({linerBackground,isShownHeaderLogo, headerTitle, onPress,ismenuShow,isbackButton,onPressHeader,powerbuttonClick,showPowerButton}) => {
  const navigation = useNavigation();
  const styles = Styles();
  const {colorTheme} = useTheme();
  return (
    // [ '#1c2231','#1c2439','#3b5998']
    <View style={styles.container}>
      <LinearGradient colors={linerBackground? linerBackground:[ '#1c2231','#1c2439','#3b5998']}>
      <View
        style={{
          flexDirection: 'row',
          // justifyContent: 'center',
          justifyContent:'flex-start',
          width: '100%',
          height: '100%',
        }}>
        <View
          style={{
            width: 50,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            zIndex:999
          }}>
          {isbackButton ? 
            <TouchableOpacity
              onPress={onPress}
              style={{padding: 5}}>
              <Icon name="arrow-back" size={25} color={colorTheme.headerTextColor} />
            </TouchableOpacity>
            :
            <></>
          }
          
          
        </View>
        <View
          style={{
            flex: 1,
            height: '100%',
            justifyContent: 'flex-start',
            alignItems: 'center',
            flexDirection:'row',
            // marginLeft: ismenuShow? -60 : -25
            // marginLeft: isShownHeaderLogo==true ? -50 : 0
            marginLeft: isbackButton==true ? -50 : -80
            
          }}>
          {isShownHeaderLogo ? (
            <Image source={Icons.applogo} style={{height:30,resizeMode:'contain'}} />
          ) : (
            <Text style={{color: colorTheme.headerTextColor, fontSize: FontSize.f16,paddingLeft:isbackButton==true ? 50 : 20}}>
              {headerTitle}
            </Text>
          )}
        </View>
        <View
          style={{
            width: 50,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
            {/* {showPowerButton ? 
              <TouchableOpacity
                // onPress={() => navigation.goBack()}
                onPress={powerbuttonClick}
                style={{padding: 5}}>
                <Icon name="power" size={20} color={colorTheme.headerTextColor} />
              </TouchableOpacity>
              :null
            } */}
            {ismenuShow?
            <TouchableOpacity
              onPress={() => {navigation.dispatch(DrawerActions.openDrawer())}}
              // onPress={onPressHeader}
              style={{padding: 5}}>
              <Icon name="menu"  size={30} color={colorTheme.headerTextColor} />
            </TouchableOpacity>
            :<></>
            }
            
          </View>
      </View>
      </LinearGradient>
    </View>
  );
};

//make this component available to the app
export default Header;
