//import liraries
import React, {Component} from 'react';
import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
import {useTheme} from '../../Constants/Theme/Theme';
import {windowHeight} from '../../Constants/window';
import Header from '../Header/Header';
import Styles from './Style';
import { ActivityIndicator } from "react-native";


// create a component
const SpinnerLoading = ({ showSpinner}) => {
  const {colorTheme} = useTheme();
  const styles = Styles();
  return (
    <View style={{flex:1,position:'absolute',top:0,height:'100%',width:'100%',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'}}>
			{showSpinner ? 
						<View style={{backgroundColor:"white",padding:5,borderRadius:50,height:50,width:50}}>
							<ActivityIndicator size='large' color={'#1c3160'} animating={true} />
						</View>
			:<></>
			}
        
    </View>
  );
};

//make this component available to the app
export default SpinnerLoading;