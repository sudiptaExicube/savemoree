import React from 'react';

import {CardStyleInterpolators, createStackNavigator} from '@react-navigation/stack';
// import {AddexpanseScreen, ApplyApplicationScreen, ApplyLeaveScreen, ApproveLeaveScreen, AttendanceScreen, CompanyLogin, DashDetails, EmployeeLogin, ExpanseScreen, Initial, LeaveBalanceScreen, LeavelistScreen, PayslipScreen, ProfileScreen} from '../Screens';
import DrawerNavigation from './DrawerNavigation';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {TouchableOpacity} from 'react-native';
import {useTheme} from '../Constants/Theme/Theme';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import { HelpScreen, Initial, LocationScreen, LoginScreen, NotificationScreen, OtpScreen, ProfileScreen, SignupScreen } from '../Screens';
import TabNavigation from './TabNavigation';

const Stack = createStackNavigator();

const MainNavigation = props => {
  const {colorTheme} = useTheme();
  const dispatch = useDispatch();
  const {isStackHeaderVisible} = useSelector(state => state.common);

  const horizontalAnimation = {
    gestureDirection: 'horizontal',
    cardStyleInterpolator: ({ current, layouts }) => {
      return {
        cardStyle: {
          transform: [
            {
              translateX: current.progress.interpolate({
                inputRange: [0, 1],
                outputRange: [layouts.screen.width, 0],
              }),
            },
          ],
        },
      };
    },
  };

  const verticalAnimation = {
    gestureDirection: 'vertical',
    cardStyleInterpolator: ({ current, layouts }) => {
      return {
        cardStyle: {
          transform: [
            {
              translateY: current.progress.interpolate({
                inputRange: [0, 1],
                outputRange: [layouts.screen.height, 0],
              }),
            },
          ],
        },
      };
    },
  };

  const verticalAnimationdd = {
    gestureDirection: 'vertical',
    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
  };

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        headerStyle: {
          backgroundColor: colorTheme.backGroundColor,
        },
        headerShadowVisible: false,
        // cardStyleInterpolator:CardStyleInterpolators.forFadeFromCenter
        // cardStyleInterpolator:CardStyleInterpolators.forHorizontalIOS
      }}>
        

      <Stack.Screen name="Initial" component={Initial} options={horizontalAnimation} />
      <Stack.Screen name="LoginScreen" component={LoginScreen} options={horizontalAnimation} />
      <Stack.Screen name="SignupScreen" component={SignupScreen} options={horizontalAnimation} />
      <Stack.Screen name="OtpScreen" component={OtpScreen} options={horizontalAnimation} />
      <Stack.Screen name="ProfileScreen" component={ProfileScreen} options={horizontalAnimation} />

      <Stack.Screen name="NotificationScreen" component={NotificationScreen} options={horizontalAnimation} />
      <Stack.Screen name="HelpScreen" component={HelpScreen} options={horizontalAnimation} />

      

      <Stack.Screen name="LocationScreen" component={LocationScreen} options={horizontalAnimation} />
      
      {/* == For showing Tabs under sidemenu == */}
      <Stack.Screen name="Home" component={DrawerNavigation} options={horizontalAnimation} /> 
      
      {/* == For showing sidemenu under tab == */}
      {/* <Stack.Screen name="Home" component={TabNavigation} />  */}
      
      
      
      
      

    </Stack.Navigator>
  );
};

// export default MainNavigation;
export { MainNavigation };
