import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer, useTheme } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Dashboard, RideTabScreen, FoodTabScreen, LoginScreen } from '../Screens';
import { useDispatch, useSelector } from 'react-redux';
import { Icon } from 'react-native-vector-icons/Ionicons';
import TabBar from '../Components/Tab/TabBar';
import DrawerNavigation from './DrawerNavigation';
import MedTabScreen from '../Screens/med-tab-screen/MedTabScreen';


const Tab = createBottomTabNavigator();

const TabNavigation = props => {
	const { colorTheme } = useTheme();
	const dispatch = useDispatch();
	const { isStackHeaderVisible } = useSelector(state => state.common);

	return (
		<Tab.Navigator
			screenOptions={{
				headerShown: false,
				headerStyle: {
					// backgroundColor: colorTheme.backGroundColor,
				},
				headerShadowVisible: false
			}}
			tabBar={props => <TabBar {...props} />}
			initialRouteName="Hometab"
		>
			{/* == For showing Tabs under sidemenu == */}
			<Tab.Screen name="Hometab" component={Dashboard} />

			{/* == For showing sidemenu under tab == */}
			{/* <Tab.Screen name="Hometab" component={DrawerNavigation} /> */}

			<Tab.Screen name="RideTabScreen" component={RideTabScreen} />
			<Tab.Screen name="FoodTabScreen" component={FoodTabScreen} />
			<Tab.Screen name="MedTabScreen" component={MedTabScreen} />
			
		</Tab.Navigator>



	)

}

export default TabNavigation;
