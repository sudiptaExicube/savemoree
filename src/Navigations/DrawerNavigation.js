import React, { useEffect } from 'react';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import { View, Text, TouchableOpacity } from 'react-native';
import { Dashboard, } from '../Screens';
import { useTheme } from '../Constants/Theme/Theme';
import { windowHeight, windowWidth } from '../Constants/window';
import Icon from 'react-native-vector-icons/Ionicons'

import { FontFamily, FontSize } from '../Constants/Fonts';
import { useSelector } from 'react-redux';
import TabNavigation from './TabNavigation';
import { LoginScreen } from '../Screens';
import { DrawerActions } from '@react-navigation/native';
const Drawer = createDrawerNavigator();
const CustomDrawerContent = props => {
  const { colorTheme } = useTheme();
  const [activeItem, setActiveitem] = React.useState('Dashboard');

  const { userData,userToken } = useSelector(state => state.common);

  useEffect(() => {
    setActiveitem('Dashboard')
  }, [activeItem]);

  clickIndividual_DrawerMenu = (screenName) => {
    setActiveitem(screenName)
    if(screenName == 'Home'){
      props.navigation.replace(screenName)
    }else{
      // props.navigation.navigate(screenName);
      props.navigation.replace(screenName);
    }

    
  }

  return (
    <DrawerContentScrollView
      showsVerticalScrollIndicator={false}
      {...props}
      style={{ backgroundColor: colorTheme.shadeDark }}>
      <View style={{ width: '100%', height: windowHeight, flexDirection: 'column' }}>
        <View style={{ height: 120, flexDirection: 'column', justifyContent: 'center' }}>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <View style={{ flexDirection: 'column', justifyContent: 'center',alignItems:'center' }}>
              <Text style={{ fontSize: 16, textAlign: 'center', fontFamily: FontFamily.bold, letterSpacing: 0.5, color: colorTheme.whiteColor }}>{userData?.name}</Text>
            </View>
          </View>
        </View>

        <View>

          <DrawerItem
            label="Dashboard"
            icon={() => <Icon color={activeItem == 'Dashboard' ? colorTheme.lightBlue : colorTheme.whiteColor} size={FontSize.f16} name={'home'} style={{ marginLeft: 10 }} />}
            labelStyle={{ color: activeItem == 'Dashboard' ? colorTheme.lightBlue : colorTheme.whiteColor, fontSize: FontSize.f15, marginLeft: 0, paddingLeft: 0 }}
            onPress={() => {
            props.navigation.dispatch(DrawerActions.toggleDrawer())
              // clickIndividual_DrawerMenu('Home')
            }}
            style={{ backgroundColor: activeItem == 'Dashboard' ? colorTheme.textUnderlineColor : null }}
          />

          <DrawerItem
            label="Profile"
            icon={() => <Icon color={activeItem == 'Profile' ? colorTheme.lightBlue : colorTheme.whiteColor} size={FontSize.f16} name={'person-outline'} style={{ marginLeft: 10 }} />}
            labelStyle={{ color: activeItem == 'Profile' ? colorTheme.lightBlue : colorTheme.whiteColor, fontSize: FontSize.f15, marginLeft: 0, paddingLeft: 0 }}
            onPress={() => {
            props.navigation.dispatch(DrawerActions.toggleDrawer())
              // clickIndividual_DrawerMenu('LoginScreen')
              clickIndividual_DrawerMenu('ProfileScreen')
            }}
            style={{ backgroundColor: activeItem == 'Profile' ? colorTheme.textUnderlineColor : null }}
          />

          {/* Extra menu */}

          {/* My Account */}
          <DrawerItem
            label="My Account"
            icon={() => <Icon color={activeItem == 'My Account' ? colorTheme.lightBlue : colorTheme.whiteColor} size={FontSize.f16} name={'person-outline'} style={{ marginLeft: 10 }} />}
            labelStyle={{ color: activeItem == 'My Account' ? colorTheme.lightBlue : colorTheme.whiteColor, fontSize: FontSize.f15, marginLeft: 0, paddingLeft: 0 }}
            onPress={() => {
            props.navigation.dispatch(DrawerActions.toggleDrawer())
              clickIndividual_DrawerMenu('NotificationScreen')
            }}
            style={{ backgroundColor: activeItem == 'My Account' ? colorTheme.textUnderlineColor : null }}
          />
          {/* My earnings */}
          <DrawerItem
            label="My earnings"
            icon={() => <Icon color={activeItem == 'My earnings' ? colorTheme.lightBlue : colorTheme.whiteColor} size={FontSize.f16} name={'cash-outline'} style={{ marginLeft: 10 }} />}
            labelStyle={{ color: activeItem == 'My earnings' ? colorTheme.lightBlue : colorTheme.whiteColor, fontSize: FontSize.f15, marginLeft: 0, paddingLeft: 0 }}
            onPress={() => {
            props.navigation.dispatch(DrawerActions.toggleDrawer())
              // clickIndividual_DrawerMenu('LoginScreen')
              clickIndividual_DrawerMenu('NotificationScreen')
            }}
            style={{ backgroundColor: activeItem == 'My earnings' ? colorTheme.textUnderlineColor : null }}
          />
          {/* Bookings history */}
          <DrawerItem
            label="Bookings history"
            icon={() => <Icon color={activeItem == 'Bookings history' ? colorTheme.lightBlue : colorTheme.whiteColor} size={FontSize.f16} name={'newspaper-outline'} style={{ marginLeft: 10 }} />}
            labelStyle={{ color: activeItem == 'Bookings history' ? colorTheme.lightBlue : colorTheme.whiteColor, fontSize: FontSize.f15, marginLeft: 0, paddingLeft: 0 }}
            onPress={() => {
            props.navigation.dispatch(DrawerActions.toggleDrawer())
              // clickIndividual_DrawerMenu('LoginScreen')
              clickIndividual_DrawerMenu('NotificationScreen')
            }}
            style={{ backgroundColor: activeItem == 'Bookings history' ? colorTheme.textUnderlineColor : null }}
          />
          {/* Subscriptions */}
          <DrawerItem
            label="Subscriptions"
            icon={() => <Icon color={activeItem == 'Subscriptions' ? colorTheme.lightBlue : colorTheme.whiteColor} size={FontSize.f16} name={'person-outline'} style={{ marginLeft: 10 }} />}
            labelStyle={{ color: activeItem == 'Subscriptions' ? colorTheme.lightBlue : colorTheme.whiteColor, fontSize: FontSize.f15, marginLeft: 0, paddingLeft: 0 }}
            onPress={() => {
            props.navigation.dispatch(DrawerActions.toggleDrawer())
              // clickIndividual_DrawerMenu('LoginScreen')
              clickIndividual_DrawerMenu('NotificationScreen')
            }}
            style={{ backgroundColor: activeItem == 'Subscriptions' ? colorTheme.textUnderlineColor : null }}
          />
          {/* My earnings */}
          <DrawerItem
            label="Discount coupons"
            icon={() => <Icon color={activeItem == 'Discount coupons' ? colorTheme.lightBlue : colorTheme.whiteColor} size={FontSize.f16} name={'person-outline'} style={{ marginLeft: 10 }} />}
            labelStyle={{ color: activeItem == 'Discount coupons' ? colorTheme.lightBlue : colorTheme.whiteColor, fontSize: FontSize.f15, marginLeft: 0, paddingLeft: 0 }}
            onPress={() => {
            props.navigation.dispatch(DrawerActions.toggleDrawer())
              // clickIndividual_DrawerMenu('LoginScreen')
              clickIndividual_DrawerMenu('NotificationScreen')
            }}
            style={{ backgroundColor: activeItem == 'Discount coupons' ? colorTheme.textUnderlineColor : null }}
          />
          {/* My Notification */}
          {/* <DrawerItem
            label="Notification"
            icon={() => <Icon color={activeItem == 'Notification' ? colorTheme.lightBlue : colorTheme.whiteColor} size={FontSize.f16} name={'notifications-outline'} style={{ marginLeft: 10 }} />}
            labelStyle={{ color: activeItem == 'Notification' ? colorTheme.lightBlue : colorTheme.whiteColor, fontSize: FontSize.f15, marginLeft: 0, paddingLeft: 0 }}
            onPress={() => {
            props.navigation.dispatch(DrawerActions.toggleDrawer())
              // clickIndividual_DrawerMenu('LoginScreen')
              clickIndividual_DrawerMenu('NotificationScreen')
            }}
            style={{ backgroundColor: activeItem == 'Notification' ? colorTheme.textUnderlineColor : null }}
          /> */}
          {/* Help */}
          <DrawerItem
            label="Help"
            icon={() => <Icon color={activeItem == 'Help' ? colorTheme.lightBlue : colorTheme.whiteColor} size={FontSize.f16} name={'help-circle-outline'} style={{ marginLeft: 10 }} />}
            labelStyle={{ color: activeItem == 'Help' ? colorTheme.lightBlue : colorTheme.whiteColor, fontSize: FontSize.f15, marginLeft: 0, paddingLeft: 0 }}
            onPress={() => {
            props.navigation.dispatch(DrawerActions.toggleDrawer())
              // clickIndividual_DrawerMenu('LoginScreen')
              clickIndividual_DrawerMenu('HelpScreen')
            }}
            style={{ backgroundColor: activeItem == 'Help' ? colorTheme.textUnderlineColor : null }}
          />


{/* My earnings */}
{/* My Account */}
{/* Bookings history */}
{/* Subscriptions */}
{/* Discount coupons */}
{/* Help */}
{/* Notification */}




        </View>


      </View>


    </DrawerContentScrollView>
  );
};


const DrawerNavigation = () => {
  const { colorTheme } = useTheme();
  return (
    <Drawer.Navigator
      // screenOptions={{
      //   // headerShadowVisible:false,
      //   headerShown:true,
      //   headerStyle: {
      //     backgroundColor: colorTheme.buttonBackgroundColor,
      //   },
      //   headerTintColor: colorTheme.textFontColor,
      //   drawerStyle: {
      //     width: '70%',
      //   },
      // }}
      screenOptions={{
        drawerPosition: 'left', headerShown: false,
        // cardStyleInterpolator:CardStyleInterpolators.forFadeFromCenter 
      }}

      drawerContent={props => <CustomDrawerContent {...props} />}>


      {/* == For showing Tabs under sidemenu == */}
      <Drawer.Screen name="Dashboard" component={TabNavigation} />

      {/* == For showing sidemenu under tab == */}
      {/* <Drawer.Screen name="Dashboard" component={Dashboard} /> */}
      
      {/* <Drawer.Screen
        name="Initial"
        component={Initial}
        options={{headerShown: false}}
      /> */}
    </Drawer.Navigator>
  );
};

export default DrawerNavigation;
