import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';

// import {
//   Colors,
//   DebugInstructions,
//   Header,
//   LearnMoreLinks,
//   ReloadInstructions,
// } from 'react-native/Libraries/NewAppScreen';

import {NavigationContainer} from '@react-navigation/native';
import {store} from './src/Store/AppStore';
import {Provider, useDispatch, useSelector} from 'react-redux';
import {ProvideTheme} from './src/Constants/Theme/Theme';
import { MainNavigation } from './src/Navigations/MainNavigation';

import { LogBox } from 'react-native';
// import MainNavigation from './src/Navigations/MainNavigation';

// import DrawerNavigation from './src/Navigations/DrawerNavigation';
// import { EventRegister } from 'react-native-event-listeners';
// import RNBootSplash from 'react-native-bootsplash';
const App = () => {
  const [isloginValue, setLoginValue] = useState(false);

  useEffect(() => {
    LogBox.ignoreLogs(['Warning: ...']); // Ignore log notification by message
    LogBox.ignoreAllLogs();
    // const listener = EventRegister.addEventListener('loginsuccess', (data) => {
    //   console.log("working event : ", data);
    //   // if(data == "true"){
    //     // setTimeout(() => {
    //       // setLoginValue(data)
    //       // RNBootSplash.hide({ fade: true });
    //       // RNBootSplash.
    //     // }, 500);
    //   // }
    // })


    // RNBootSplash.show();
  // SplashScreen.hide();
    
  }, [isloginValue]);

  return (

      // <View>
      //   <Text>Hello world</Text>
      // </View>


      <Provider store={store}>
       <ProvideTheme>
       <NavigationContainer >
         <MainNavigation />      
         </NavigationContainer>
       </ProvideTheme>
     </Provider>
     
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;